#ifndef __SHA2_H__
#define __SHA2_H__

/*
 * FIPS 180-2 SHA-224/256/384/512 implementation
 * Copyright (C) 2023, Saeed Rezaee <saeed@rezaee.net>
 * All rights reserved.
 */
 
 #include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

#define SHA224_DIGEST_SIZE ( 224 / 8 )
#define SHA256_DIGEST_SIZE ( 256 / 8 )
#define SHA384_DIGEST_SIZE ( 384 / 8 )
#define SHA512_DIGEST_SIZE ( 512 / 8 )

#define SHA256_BLOCK_SIZE  (  512 / 8 )
#define SHA512_BLOCK_SIZE  ( 1024 / 8 )
#define SHA384_BLOCK_SIZE  SHA512_BLOCK_SIZE
#define SHA224_BLOCK_SIZE  SHA256_BLOCK_SIZE


#define RSH(x, n)    ( x >> n )
#define LSH(x, n)    ( x << n )
#define ROR(x, n)    ( ( x >> n ) | ( x << ( ( sizeof ( x ) << 3 ) - n ) ) )
#define ROL(x, n)    ( ( x << n ) | ( x >> ( ( sizeof ( x ) << 3 ) - n ) ) )
#define CHOICE(x, y, z)  ( ( x & y  ) ^ ( ~x & z ) )
#define MAJORITY(x, y, z) ( ( x & y  ) ^ (  x & z ) ^ ( y & z ) )

#define SHA256_F1(x) ( ROR ( x,  2 ) ^ ROR ( x, 13 ) ^ ROR ( x, 22 ) )
#define SHA256_F2(x) ( ROR ( x,  6 ) ^ ROR ( x, 11 ) ^ ROR ( x, 25 ) )
#define SHA256_F3(x) ( ROR ( x,  7 ) ^ ROR ( x, 18 ) ^ RSH ( x,  3 ) )
#define SHA256_F4(x) ( ROR ( x, 17 ) ^ ROR ( x, 19 ) ^ RSH ( x, 10 ) )

#define SHA512_F1(x) ( ROR ( x, 28 ) ^ ROR ( x, 34 ) ^ ROR ( x, 39 ) )
#define SHA512_F2(x) ( ROR ( x, 14 ) ^ ROR ( x, 18 ) ^ ROR ( x, 41 ) )
#define SHA512_F3(x) ( ROR ( x,  1 ) ^ ROR ( x,  8 ) ^ RSH ( x,  7 ) )
#define SHA512_F4(x) ( ROR ( x, 19 ) ^ ROR ( x, 61 ) ^ RSH ( x,  6 ) )

#define UNPACK32(x, s)                            \
{                                                 \
    *( ( s ) + 3 ) = ( uint8_t ) ( ( x )       ); \
    *( ( s ) + 2 ) = ( uint8_t ) ( ( x ) >>  8 ); \
    *( ( s ) + 1 ) = ( uint8_t ) ( ( x ) >> 16 ); \
    *( ( s ) + 0 ) = ( uint8_t ) ( ( x ) >> 24 ); \
}

#define PACK32(s, x)                                \
{                                                   \
    *(x) =   ( ( uint32_t ) *( ( s ) + 3 )       )  \
           | ( ( uint32_t ) *( ( s ) + 2 ) <<  8 )  \
           | ( ( uint32_t ) *( ( s ) + 1 ) << 16 )  \
           | ( ( uint32_t ) *( ( s ) + 0 ) << 24 ); \
}

#define UNPACK64(x, s)                            \
{                                                 \
    *( ( s ) + 7 ) = ( uint8_t ) ( ( x )       ); \
    *( ( s ) + 6 ) = ( uint8_t ) ( ( x ) >>  8 ); \
    *( ( s ) + 5 ) = ( uint8_t ) ( ( x ) >> 16 ); \
    *( ( s ) + 4 ) = ( uint8_t ) ( ( x ) >> 24 ); \
    *( ( s ) + 3 ) = ( uint8_t ) ( ( x ) >> 32 ); \
    *( ( s ) + 2 ) = ( uint8_t ) ( ( x ) >> 40 ); \
    *( ( s ) + 1 ) = ( uint8_t ) ( ( x ) >> 48 ); \
    *( ( s ) + 0 ) = ( uint8_t ) ( ( x ) >> 56 ); \
}

#define PACK64(s, x)                                \
{                                                   \
    *(x) =   ( ( uint64_t ) *( ( s ) + 7 )       )  \
           | ( ( uint64_t ) *( ( s ) + 6 ) <<  8 )  \
           | ( ( uint64_t ) *( ( s ) + 5 ) << 16 )  \
           | ( ( uint64_t ) *( ( s ) + 4 ) << 24 )  \
           | ( ( uint64_t ) *( ( s ) + 3 ) << 32 )  \
           | ( ( uint64_t ) *( ( s ) + 2 ) << 40 )  \
           | ( ( uint64_t ) *( ( s ) + 1 ) << 48 )  \
           | ( ( uint64_t ) *( ( s ) + 0 ) << 56 ); \
}


typedef struct sha256_t 
{
    uint32_t total_length;
    uint32_t length;
    uint8_t  block [ 2 * SHA256_BLOCK_SIZE ];
    uint32_t h     [ 8 ];
	
} sha256_t;

typedef struct sha512_t 
{
    uint32_t total_length;
    uint32_t length;
    uint8_t  block [ 2 * SHA512_BLOCK_SIZE ];
    uint64_t h     [ 8 ];
	
} sha512_t;

typedef sha512_t sha384_t;
typedef sha256_t sha224_t;


// The second 32 bits of the fractional parts of the square roots of the 9th through 16th primes 23..53
// long double primes [ 8 ] = { 23, 29, 31, 37, 41, 43, 47, 53 };

void sha224_generate_initial_hash ( uint32_t output [ 8 ] )
{
	long double primes [ 8 ] = { 23, 29, 31, 37, 41, 43, 47, 53 };	
	for ( uint32_t i = 0; i < 8; i++ )
	{	
        output [ i ] = ( uint32_t ) ( fmodl ( fmodl ( sqrtl ( primes [ i ] ), 1 ) * powl ( 16, 8 ), 1 ) * powl ( 16, 8 ) );
	}
}

uint32_t sha224_initial_hash_values [ 8 ] =
{
	0xC1059ED8, 
	0x367CD507, 
	0x3070DD17, 
	0xF70E5939,
    0xFFC00B31, 
	0x68581511, 
	0x64F98FA7, 
	0xBEFA4FA4
};


// first 32 bits of the fractional parts of the square roots of the first 8 primes 2..19
// float primes [ 8 ] = { 2, 3, 5, 7, 11, 13, 17, 19 };
void sha256_generate_initial_hash ( uint32_t output [ 8 ] )
{
	long double first_8_primes [ 8 ] = { 2, 3, 5, 7, 11, 13, 17, 19 };	
	for ( uint32_t i = 0; i < 8; i++ )
	{	
        output [ i ] = ( uint32_t ) ( fmodl ( sqrtl ( first_8_primes [ i ] ), 1 ) * powl ( 16, 8 ) );
	}
}

uint32_t sha256_initial_hash_values [ 8 ] =
{
	0x6A09E667, 
	0xBB67AE85, 
	0x3C6EF372, 
	0xA54FF53A,
	0x510E527F, 
	0x9B05688C, 
	0x1F83D9AB, 
	0x5BE0CD19
};


// he initial hash values h0 through h7 are different (taken from the 9th through 16th primes), and
// the output is constructed by omitting h6 and h7
uint64_t sha384_initial_hash_values [ 8 ] =
{
	0xCBBB9D5DC1059ED8ULL, 
	0x629A292A367CD507ULL,
    0x9159015A3070DD17ULL, 
	0x152FECD8F70E5939ULL,
    0x67332667FFC00B31ULL, 
	0x8EB44A8768581511ULL,
    0xDB0C2E0D64F98FA7ULL, 
	0x47B5481DBEFA4FA4ULL
};

uint64_t sha512_initial_hash_values [ 8 ] =
{
	0x6A09E667F3BCC908ULL, 
	0xBB67AE8584CAA73BULL,
    0x3C6EF372FE94F82BULL, 
	0xA54FF53A5F1D36F1ULL,
    0x510E527FADE682D1ULL, 
	0x9B05688C2B3E6C1FULL,
    0x1F83D9ABFB41BD6BULL, 
	0x5BE0CD19137E2179ULL
};

// first 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311
/* uint32_t primes [ 64 ] = 
 * {
 *		2,   3,   5,   7,   11,  13,  17,  19,   
 *	    23,  29,  31,  37,  41,  43,  47,  53, 
 *		59,  61,  67,  71,  73,  79,  83,  89,   
 *		97,  101, 103, 107, 109, 113, 127, 131, 
 *	 	137, 139, 149, 151, 157, 163, 167, 173, 
 *		179, 181, 191, 193, 197, 199, 211, 223, 
 *	 	227, 229, 233, 239, 241, 251, 257, 263, 
 *	    269, 271, 277, 281, 283, 293, 307, 311
 * };
 */
 
void sha256_generate_round_constants ( uint32_t output [ 64 ] )
{
	uint32_t first_64_primes [ 64 ] = 
	{
 		2,   3,   5,   7,   11,  13,  17,  19,   
 	    23,  29,  31,  37,  41,  43,  47,  53, 
 		59,  61,  67,  71,  73,  79,  83,  89,   
 		97,  101, 103, 107, 109, 113, 127, 131, 
 	 	137, 139, 149, 151, 157, 163, 167, 173, 
 		179, 181, 191, 193, 197, 199, 211, 223, 
 	 	227, 229, 233, 239, 241, 251, 257, 263, 
 	    269, 271, 277, 281, 283, 293, 307, 311
	};
	
	for ( uint32_t i = 0; i < 64; i++ )
	{	
        output [ i ] = ( uint32_t ) ( fmod ( cbrt ( first_64_primes [ i ] ), 1 ) * pow ( 16, 8 ) );
	}
}


uint32_t sha256_round_constants [ 64 ] =
{
	0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
    0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
    0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
    0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
    0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
    0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
    0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
    0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
    0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
    0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
    0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
    0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
    0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
    0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
    0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
    0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
};

uint64_t sha512_round_constants [ 80 ] =
{
	0x428A2F98D728AE22ULL, 0x7137449123EF65CDULL,
    0xB5C0FBCFEC4D3B2FULL, 0xE9B5DBA58189DBBCULL,
    0x3956C25BF348B538ULL, 0x59F111F1B605D019ULL,
    0x923F82A4AF194F9BULL, 0xAB1C5ED5DA6D8118ULL,
    0xD807AA98A3030242ULL, 0x12835B0145706FBEULL,
    0x243185BE4EE4B28CULL, 0x550C7DC3D5FFB4E2ULL,
    0x72BE5D74F27B896FULL, 0x80DEB1FE3B1696B1ULL,
    0x9BDC06A725C71235ULL, 0xC19BF174CF692694ULL,
    0xE49B69C19EF14AD2ULL, 0xEFBE4786384F25E3ULL,
    0x0FC19DC68B8CD5B5ULL, 0x240CA1CC77AC9C65ULL,
    0x2DE92C6F592B0275ULL, 0x4A7484AA6EA6E483ULL,
    0x5CB0A9DCBD41FBD4ULL, 0x76F988DA831153B5ULL,
    0x983E5152EE66DFABULL, 0xA831C66D2DB43210ULL,
    0xB00327C898FB213FULL, 0xBF597FC7BEEF0EE4ULL,
    0xC6E00BF33DA88FC2ULL, 0xD5A79147930AA725ULL,
    0x06CA6351E003826FULL, 0x142929670A0E6E70ULL,
    0x27B70A8546D22FFCULL, 0x2E1B21385C26C926ULL,
    0x4D2C6DFC5AC42AEDULL, 0x53380D139D95B3DFULL,
    0x650A73548BAF63DEULL, 0x766A0ABB3C77B2A8ULL,
    0x81C2C92E47EDAEE6ULL, 0x92722C851482353BULL,
    0xA2BFE8A14CF10364ULL, 0xA81A664BBC423001ULL,
    0xC24B8B70D0F89791ULL, 0xC76C51A30654BE30ULL,
    0xD192E819D6EF5218ULL, 0xD69906245565A910ULL,
    0xF40E35855771202AULL, 0x106AA07032BBD1B8ULL,
    0x19A4C116B8D2D0C8ULL, 0x1E376C085141AB53ULL,
    0x2748774CDF8EEB99ULL, 0x34B0BCB5E19B48A8ULL,
    0x391C0CB3C5C95A63ULL, 0x4ED8AA4AE3418ACBULL,
    0x5B9CCA4F7763E373ULL, 0x682E6FF3D6B2B8A3ULL,
    0x748F82EE5DEFB2FCULL, 0x78A5636F43172F60ULL,
    0x84C87814A1F0AB72ULL, 0x8CC702081A6439ECULL,
    0x90BEFFFA23631E28ULL, 0xA4506CEBDE82BDE9ULL,
    0xBEF9A3F7B2C67915ULL, 0xC67178F2E372532BULL,
    0xCA273ECEEA26619CULL, 0xD186B8C721C0C207ULL,
    0xEADA7DD6CDE0EB1EULL, 0xF57D4F7FEE6ED178ULL,
    0x06F067AA72176FBAULL, 0x0A637DC5A2C898A6ULL,
    0x113F9804BEF90DAEULL, 0x1B710B35131C471BULL,
    0x28DB77F523047D84ULL, 0x32CAAB7B40C72493ULL,
    0x3C9EBE0A15C9BEBCULL, 0x431D67C49C100D4CULL,
    0x4CC5D4BECB3E42B6ULL, 0x597F299CFC657E2AULL,
    0x5FCB6FAB3AD6FAECULL, 0x6C44198C4A475817ULL
};


static void sha256_transform ( sha256_t *ctx, const uint8_t *message, uint32_t number_of_blocks )
{
    uint32_t w  [ 64 ]   = { 0 };
    uint32_t wv [ 8  ]   = { 0 };
    uint32_t t1          = 0;
	uint32_t t2          = 0;
    const uint8_t *block = 0;
    uint32_t i           = 0;
    uint32_t j           = 0;

    for ( i = 0; i < number_of_blocks; i++ ) 
	{
        block = message + ( i << 6 );

        for ( j = 0; j < 16; j++ ) 
		{
            PACK32 ( &block [ j << 2 ], &w [ j ] );
        }

        for ( j = 16; j < 64; j++ ) 
		{
			w [ j ] =  SHA256_F4 ( w [ j -  2 ] ) + w [ j -  7] + SHA256_F3 ( w [ j - 15 ] ) + w [ j - 16 ];
        }

        for ( j = 0; j < 8; j++ ) 
		{
            wv [ j ] = ctx->h [ j ];
        }

        for ( j = 0; j < 64; j++ ) 
		{
            t1 = wv [ 7 ] + SHA256_F2 ( wv [ 4 ] ) + CHOICE ( wv [ 4 ], wv [ 5 ], wv [ 6 ] ) + sha256_round_constants [ j ] + w [ j ];
            t2 = SHA256_F1 ( wv [ 0 ] ) + MAJORITY ( wv [ 0 ], wv [ 1 ], wv [ 2 ] );
            wv [ 7 ] = wv [ 6 ];
            wv [ 6 ] = wv [ 5 ];
            wv [ 5 ] = wv [ 4 ];
            wv [ 4 ] = wv [ 3 ] + t1;
            wv [ 3 ] = wv [ 2 ];
            wv [ 2 ] = wv [ 1 ];
            wv [ 1 ] = wv [ 0 ];
            wv [ 0 ] = t1 + t2;
        }       

        for ( j = 0; j < 8; j++ ) 
		{
            ctx->h [ j ] += wv [ j ];
        }
    }
}


void sha512_transform ( sha512_t *ctx, const uint8_t *message, uint32_t number_of_blocks )
{
    uint64_t w  [ 80 ]   = { 0 };
    uint64_t wv [ 8  ]   = { 0 };
    uint64_t t1          = 0;
	uint64_t t2          = 0;
    const uint8_t *block = 0;
    uint32_t i           = 0;
	uint32_t j			 = 0;

    for ( i = 0; i < number_of_blocks; i++ ) 
	{
        block = message + ( i << 7 );

		for ( j = 0; j < 16; j++ ) 
		{
            PACK64 ( &block [ j << 3 ], &w [ j ] );
        }

        for ( j = 16; j < 80; j++ ) 
		{
			w [ j ] =  SHA512_F4 ( w [ j -  2 ] ) + w [ j -  7 ] + SHA512_F3 ( w [ j - 15 ] ) + w [ j - 16 ];
        }

        for ( j = 0; j < 8; j++ ) 
		{
            wv [ j ] = ctx->h [ j ];
        }

        for ( j = 0; j < 80; j++ ) 
		{
            t1 = wv [ 7 ] + SHA512_F2 ( wv [ 4 ] ) + CHOICE ( wv [ 4 ], wv [ 5 ], wv [ 6 ] ) + sha512_round_constants [ j ] + w [ j ];
            t2 = SHA512_F1 ( wv [ 0 ] ) + MAJORITY ( wv [ 0 ], wv [ 1 ], wv [ 2 ] );
            wv [ 7 ] = wv [ 6 ];
            wv [ 6 ] = wv [ 5 ];
            wv [ 5 ] = wv [ 4 ];
            wv [ 4 ] = wv [ 3 ] + t1;
            wv [ 3 ] = wv [ 2 ];
            wv [ 2 ] = wv [ 1 ];
            wv [ 1 ] = wv [ 0 ];
            wv [ 0 ] = t1 + t2;
        }

        for ( j = 0; j < 8; j++ ) 
		{
            ctx->h [ j ] += wv [ j ];
        }
    }
}

void sha224_init ( sha224_t *ctx )
{
	uint32_t i;
    for ( i = 0; i < 8; i++ ) 
	{
        ctx->h [ i ] = sha224_initial_hash_values [ i ];
    }
	ctx->length       = 0;
    ctx->total_length = 0;
	return;
}

void sha224_update ( sha224_t *ctx, const uint8_t *message, uint32_t length )
{
	uint32_t number_of_blocks      = 0;
    uint32_t new_length            = 0;
	uint32_t remained_length       = 0;
	uint32_t tmporary_length       = 0;
    const uint8_t *shifted_message = 0;

    tmporary_length = SHA224_BLOCK_SIZE - ctx->length;
    remained_length = length < tmporary_length ? length : tmporary_length;
	
    memcpy ( &ctx->block [ ctx->length ], message, remained_length );

    if ( ctx->length + length < SHA224_BLOCK_SIZE ) 
	{
        ctx->length += length;
        return;
    }

    new_length         = length - remained_length;
    number_of_blocks   = new_length / SHA224_BLOCK_SIZE;
    shifted_message    = message + remained_length;
	
    sha256_transform ( ctx, ctx->block, 1 );
    sha256_transform ( ctx, shifted_message, number_of_blocks );
	
    remained_length    = new_length % SHA224_BLOCK_SIZE;
    memcpy ( ctx->block, &shifted_message [ number_of_blocks << 6 ], remained_length );
	
    ctx->length        = remained_length;
    ctx->total_length += ( number_of_blocks + 1 ) << 6;
	return;
}
void sha224_final ( sha224_t *ctx, uint8_t *digest )
{
	uint32_t number_of_blocks = 0;
    uint32_t pm_len           = 0;
    uint32_t len_b            = 0;
    uint32_t i                = 0;

    number_of_blocks = ( 1 + ( ( SHA224_BLOCK_SIZE - 9 ) < ( ctx->length % SHA224_BLOCK_SIZE ) ) );

    len_b = ( ctx->total_length + ctx->length ) << 3;
    pm_len = number_of_blocks << 6;

    memset ( ctx->block + ctx->length, 0, pm_len - ctx->length );
    ctx->block [ ctx->length ] = 0x80;
    UNPACK32 ( len_b, ctx->block + pm_len - 4 );

    sha256_transform ( ctx, ctx->block, number_of_blocks );

    for ( i = 0 ; i < 7; i++ ) 
	{
        UNPACK32 ( ctx->h [ i ], &digest [ i << 2 ] );
    }
	return;
}

void sha224 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha224_t ctx = { 0 };	
	sha224_init    ( &ctx );
	sha224_update  ( &ctx, message, length );
	sha224_final   ( &ctx, digest );
	return;
}

void sha256_init ( sha256_t *ctx )
{
    uint32_t i;
    for ( i = 0; i < 8; i++ ) 
	{
        ctx->h [ i ] = sha256_initial_hash_values [ i ];
    }
    ctx->length       = 0;
    ctx->total_length = 0;
}

void sha256_update ( sha256_t *ctx, const uint8_t *message, uint32_t length )
{
	uint32_t number_of_blocks      = 0;
    uint32_t new_length            = 0;
	uint32_t remained_length       = 0;
	uint32_t temporary_length      = 0;	
    const uint8_t* shifted_message = 0;

    temporary_length = SHA256_BLOCK_SIZE - ctx->length;
    remained_length  = length < temporary_length ? length : temporary_length;
    memcpy ( &ctx->block [ ctx->length ], message, remained_length );

    if ( ctx->length + length < SHA256_BLOCK_SIZE ) 
	{
        ctx->length += length;
        return;
    }

    new_length       = length     - remained_length;
    number_of_blocks = new_length / SHA256_BLOCK_SIZE;
    shifted_message  = message    + remained_length;
   
	sha256_transform ( ctx, ctx->block, 1 );
    sha256_transform ( ctx, shifted_message, number_of_blocks );
	
    remained_length  = new_length % SHA256_BLOCK_SIZE;

    memcpy ( ctx->block, &shifted_message [ number_of_blocks << 6 ], remained_length );

    ctx->length        = remained_length;
    ctx->total_length += ( number_of_blocks + 1 ) << 6;
}

void sha256_final ( sha256_t *ctx, uint8_t *digest )
{
	uint32_t number_of_blocks = 0;
    uint32_t pm_length        = 0;
    uint32_t block_length     = 0;
	uint32_t i                = 0;

    number_of_blocks = ( 1 + ( ( SHA256_BLOCK_SIZE - 9 ) < ( ctx->length % SHA256_BLOCK_SIZE ) ) );

    block_length = ( ctx->total_length + ctx->length ) << 3;
    pm_length    = number_of_blocks << 6;

    memset   ( ctx->block + ctx->length, 0, pm_length - ctx->length );
    ctx->block [ ctx->length ] = 0x80;
    UNPACK32 ( block_length, ctx->block + pm_length - 4 );

    sha256_transform ( ctx, ctx->block, number_of_blocks );

    for ( i = 0 ; i < 8; i++ )
	{
        UNPACK32 ( ctx->h [ i ], &digest [ i << 2 ] );
    }
}

void sha256 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha256_t ctx = { 0 };
	sha256_init    ( &ctx );
	sha256_update  ( &ctx, message, length );
	sha256_final   ( &ctx, digest );
	return;
}

void sha384_init ( sha384_t *ctx )
{
    uint32_t i;
    for ( i = 0; i < 8; i++ )
	{
        ctx->h [ i ]  = sha384_initial_hash_values [ i ];
    }
    ctx->length       = 0;
    ctx->total_length = 0;
}

void sha384_update ( sha384_t *ctx, const uint8_t *message, uint32_t length )
{
	uint32_t number_of_blocks      = 0;
    uint32_t new_length            = 0;
	uint32_t remained_length       = 0;
	uint32_t temporary_length      = 0;
    const uint8_t *shifted_message = 0;

    temporary_length = SHA384_BLOCK_SIZE - ctx->length;
    remained_length  = length < temporary_length ? length : temporary_length;

    memcpy ( &ctx->block [ ctx->length ], message, remained_length );

    if ( ctx->length + length < SHA384_BLOCK_SIZE ) 
	{
        ctx->length += length;
        return;
    }

    new_length       = length - remained_length;
    number_of_blocks = new_length / SHA384_BLOCK_SIZE;

    shifted_message = message + remained_length;

    sha512_transform ( ctx, ctx->block, 1 );
    sha512_transform ( ctx, shifted_message, number_of_blocks );

    remained_length    = new_length % SHA384_BLOCK_SIZE;

    memcpy ( ctx->block, &shifted_message [ number_of_blocks << 7 ], remained_length );

    ctx->length        = remained_length;
    ctx->total_length += ( number_of_blocks + 1 ) << 7;
}

void sha384_final ( sha384_t *ctx, uint8_t *digest )
{
	uint32_t number_of_blocks = 0;
    uint32_t pm_length        = 0;
    uint32_t block_length     = 0;
	uint32_t i                = 0;
	
    number_of_blocks = ( 1 + ( ( SHA384_BLOCK_SIZE - 17 ) < ( ctx->length % SHA384_BLOCK_SIZE ) ) );

    block_length = ( ctx->total_length + ctx->length ) << 3;
    pm_length = number_of_blocks << 7;
    memset ( ctx->block + ctx->length, 0, pm_length - ctx->length );
    ctx->block [ ctx->length ] = 0x80;
	
    UNPACK32 ( block_length, ctx->block + pm_length - 4 );
    sha512_transform ( ctx, ctx->block, number_of_blocks );

    for ( i = 0 ; i < 6; i++ ) 
	{
        UNPACK64 ( ctx->h [ i ], &digest [ i << 3 ] );
    }
}

void sha384 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha384_t ctx = { 0 };
	sha384_init    ( &ctx                  );
	sha384_update  ( &ctx, message, length );
	sha384_final   ( &ctx, digest          );
	return;
}

void sha512_init ( sha512_t *ctx )
{
    uint32_t i;
    for ( i = 0; i < 8; i++ )
	{
        ctx->h [ i ] = sha512_initial_hash_values [ i ];
    }
    ctx->length       = 0;
    ctx->total_length = 0;
}

void sha512_update ( sha512_t *ctx, const uint8_t *message, uint32_t length )
{
	uint32_t number_of_blocks      = 0;
    uint32_t new_length            = 0;
	uint32_t remained_length       = 0;
	uint32_t temporary_length      = 0;
    const uint8_t *shifted_message = 0;

    temporary_length = SHA512_BLOCK_SIZE - ctx->length;
    remained_length  = length < temporary_length ? length : temporary_length;

    memcpy ( &ctx->block [ ctx->length ], message, remained_length );

    if ( ctx->length + length < SHA512_BLOCK_SIZE ) 
	{
        ctx->length += length;
        return;
    }

    new_length       = length - remained_length;
    number_of_blocks = new_length / SHA512_BLOCK_SIZE;

    shifted_message = message + remained_length;

    sha512_transform ( ctx, ctx->block, 1 );
    sha512_transform ( ctx, shifted_message, number_of_blocks );

    remained_length = new_length % SHA512_BLOCK_SIZE;

    memcpy ( ctx->block, &shifted_message [ number_of_blocks << 7 ], remained_length );

    ctx->length        = remained_length;
    ctx->total_length += ( number_of_blocks + 1 ) << 7;
}

void sha512_final ( sha512_t *ctx, uint8_t *digest )
{
	uint32_t number_of_blocks = 0;
    uint32_t pm_length        = 0;
    uint32_t block_length     = 0;
	uint32_t i                = 0;
	
    number_of_blocks = 1 + ( ( SHA512_BLOCK_SIZE - 17 ) < ( ctx->length % SHA512_BLOCK_SIZE ) );

    block_length = ( ctx->total_length + ctx->length ) << 3;
    pm_length = number_of_blocks << 7;

    memset     ( ctx->block + ctx->length, 0, pm_length - ctx->length );
    ctx->block [ ctx->length ] = 0x80;
    UNPACK32   ( block_length, ctx->block + pm_length - 4 );

    sha512_transform ( ctx, ctx->block, number_of_blocks );

    for ( i = 0 ; i < 8; i++ ) 
	{
        UNPACK64 ( ctx->h [ i ], &digest [ i << 3 ] );
    }
}

void sha512 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha512_t ctx = { 0 };
	sha512_init    ( &ctx                  );
	sha512_update  ( &ctx, message, length );
	sha512_final   ( &ctx, digest          );
	return;
}

void sha512_224_init ( sha512_t *ctx )
{	

}

void sha512_224_update ( sha512_t *ctx, const uint8_t *message, uint32_t length )
{
	
}

void sha512_224_final ( sha512_t *ctx, uint8_t *digest )
{
	
}

void sha512_224 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha512_t ctx =     { 0 };
	sha512_224_init    ( &ctx                  );
	sha512_224_update  ( &ctx, message, length );
	sha512_224_final   ( &ctx, digest          );
	return;
}


void sha512_256_init ( sha512_t *ctx )
{
	return;
}

void sha512_256_update ( sha512_t *ctx, const uint8_t *message, uint32_t length )
{
	return;
}

void sha512_256_final ( sha512_t *ctx, uint8_t *digest )
{
	return;
}

void sha512_256 ( const uint8_t *message, uint32_t length, uint8_t *digest )
{
	sha512_t ctx =     { 0 };
	sha512_256_init    ( &ctx                  );
	sha512_256_update  ( &ctx, message, length );
	sha512_256_final   ( &ctx, digest          );
	return;
}


#ifdef __cplusplus
}
#endif

#endif /* !__SHA2_H__ */