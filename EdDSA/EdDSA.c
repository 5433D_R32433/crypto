#include <stdio.h>
#include <gmp.h>
#include <stdint.h>


void random ( mpz_t *r, mpz_t q )
{
	gmp_randstate_t state;
	gmp_randinit_mt ( state );
	
	mpz_t t;
	mpz_init ( t );
	mpz_sub_ui  ( t, q, 1 );
	mpz_urandomm ( *r, state, t );
}




void print_openssl_like ( mpz_t x, const char *name )
{
	char buffer [ 4096 ] = { 0 };
	buffer [ 0 ] = '0';
	mpz_get_str ( buffer + 1, 16, x );
	
	printf ( "%s:\n", name );
	printf ( "    " );
	int nl = 0;
	for ( int i = 0; i < strlen ( buffer ); i += 2 )
	{
		if ( nl == 15 )
		{
			nl = 0;
			printf ( "\n    " );
		}
		printf ( "%c%c:", toupper ( buffer [ i ] ), toupper ( buffer [ i + 1 ] ) );
		++nl;
	}
}


 // g = h ^ ( p − 1 ) / q mod p
void gendsa ( mpz_t p, mpz_t q )
{
	// First Phase
	mpz_t t;
	mpz_init ( t );
	
	mpz_t g;
	mpz_init ( g );
	
	mpz_sub_ui ( t, p, 1 );
	mpz_div    ( t, t, q );
	
	mpz_t h;
	mpz_init_set_ui ( h, 2 );
	
	mpz_powm ( g, h, t, p );

	printf ( "\nG(16): %s\n", mpz_get_str ( 0, 16, g ) );
	printf ( "\nG(10): %s\n", mpz_get_str ( 0, 10, g ) );
	
	print_openssl_like ( g, "G" );
	
	// Second Phase
	
	mpz_t x;
	mpz_init_set_str ( x, "0x6da903373984454f944d39ccfb95ee6843a5dc27307f3cc9e2f853b3", 0 );
	
	mpz_t y;
	mpz_init ( y );
	
	mpz_powm ( y, g, x, p );
	
	
	printf ( "\n\nPUB: %s\n", mpz_get_str ( 0, 16, y ) );
	
}


int main ( int argc, char **argv )
{
	mpz_t p;
	mpz_init_set_str ( p, "0x009be7734a15494b8370aa6fc59979cababe99454db78eab3d112f8cf299504435bd93f641eb729fc997f16acc2b7e2b9d167803a527fd7060d6692b83489b0c0f62b953fadd240b811e612570a06a5813b2c16ffebd7d6c27d90422d847b5b698a43ec622105e6560e933bc697c42a3fce1a1104cbe99ee1dd720ee567e6a80e99b10e616ed0c64497fde158671e3b88e6d72a3a8b42261ad2f2ab1a570a61ddeabe9141a4b4b3151031dadc6c8788096b058f43fae1c6d63f1fca56932b358d33e4335a2cccac869663273dc3ff256211083a3ebf038d82565693e0a644b004ea0b3219a8b6c9548901b58479bbf1cf682d751f964f78171e5e6a3c28a1bc1f5", 0 );

    printf ( "PRIV: %s\n", mpz_get_str ( 0, 10, p ) );

	long bits = mpz_sizeinbase ( p, 2 );
	printf ( "P = %d\n", bits );
	
	mpz_t q;
	mpz_init_set_str ( q, "0x00b40dce1f259e1e98610855c70ef904df2df57849c2d028491386c37d", 0 );
	
	bits = mpz_sizeinbase ( q, 2 );
	printf ( "Q = %d\n", bits );

    printf ( "PUB: %s\n", mpz_get_str ( 0, 10, q ) );




	mpz_t g;
	mpz_init_set_str ( g, "0x08a4cb232e357d1fa27153e010a31654fa91e76f0c0e45ac4775c87da27c5b9f8bd3f5f6b35ff6b5431d55b4049baa89d5c4a6459c0b44ac7b236eb693ddc294ba5a77fbc117f168189b451994efdecf3d96e50d8bc7a5ba8a2d256cbc5d9f3797f7416fb23ff4e5b29473124a0ac23ff2ca044c1e17b24e795110ec637faded2d1017cc70e9cd4510e24c52838a2aad0a22514927277af3269435845af5ad5e521970f0c96855da09a3fc71ebaa1c1320b497abbe486c18ceb6fcb3cc9156e440dd23bf7bd310ad56b43a64ae0a647201ccc7b3e3f8945ecb6f53c93417250d15b10128f539cac9bb85332f130ef50c8f82a3fb942efa2a936752c513d34919", 0 );
	
	gendsa ( p, q );
    
    return 0;
}


    
