#include "sha2.h"
#include <stdio.h>
#include <stdlib.h>

#if defined(__GNUC__) || defined(__clang__)
#define FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define FORCEINLINE __forceinline
#else
#define FORCEINLINE inline
#endif

#if defined(_MSC_VER)

FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return _byteswap_ushort ( x ); }
FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return _byteswap_ulong  ( x ); }
FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return _byteswap_uint64 ( x ); }

#elif defined (__GNUC__) || defined (__clang__)

FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return __builtin_bswap16 ( x ); }
FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return __builtin_bswap32 ( x ); }
FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return __builtin_bswap64 ( x ); }

#else
	
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )
	
#endif


void bitcoin_checksum ( uint8_t *data, uint32_t length, uint8_t *checksum )
{
	uint8_t digest [ SHA256_DIGEST_SIZE ] = { 0 };
	sha256 ( data, length, digest );
	sha256 ( digest, SHA256_DIGEST_SIZE, digest );
	
	memcpy ( checksum, data, length );
	memcpy ( checksum + length, digest, 4 );
	
	printf ( "Checksum: " );
	for ( uint32_t i = 0; i < ( length + 4 ); i++ )
	{
		printf ( "%02X", checksum [ i ] );
	}
	printf ( "\n" );
}

void bitcoin_wif_private_key ( uint8_t *pk, uint32_t pk_length, bool mainnet, bool compressed, uint8_t *wif )
{
	
}


int main ( int argc, char **argv )
{
	// int8_t *str = "The quick brown fox jumps over the lazy dog";
	uint8_t data [ ] = { 0xdc, 0x8d, 0x4e, 0x09, 0x76, 0x85, 0x6d, 0x10, 0xb7, 0x53, 0x0e, 0x71, 0x09, 0x23, 0xe9, 0x41, 0xd7, 0x3d, 0x52, 0xae };
	uint8_t checksum [ 2048 ] = { 0 };
	
	bitcoin_checksum ( data, sizeof ( data ), checksum );
	
	return 0;
}