#ifndef __RIPEMD160_H__
#define __RIPEMD160_H__

/*
 * RIPEMD16 implementation
 * Copyright (C) 2023, Saeed Rezaee <saeed@rezaee.net>
 * All rights reserved.
 */
 
 #include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

#define RIPEMD160_BLOCK_LENGTH  64
#define RIPEMD160_DIGEST_LENGTH 20

typedef struct ripemd16_t
{
	uint32_t total [ 2 ];                      /* number of bytes processed  */
	uint32_t state [ 5 ];                      /* intermediate digest state  */
	uint8_t buffer [ RIPEMD160_BLOCK_LENGTH ]; /* data block being processed */
  
} ripemd16_t;

void ripemd160_init   ( ripemd16_t *ctx );
void ripemd160_update ( ripemd16_t *ctx, const uint8_t *message, uint32_t length );
void ripemd160_final  ( ripemd16_t *ctx, uint8_t digest [RIPEMD160_DIGEST_LENGTH ] );
void ripemd160        ( const uint8_t *message, uint32_t length, uint8_t digest [ RIPEMD160_DIGEST_LENGTH ] );


#ifdef __cplusplus
}
#endif

#endif /* !__RIPEMD160_H__ */