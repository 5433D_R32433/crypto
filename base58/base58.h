#ifndef __BASE58_H__
#define __BASE58_H__


#include <stdint.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

const int8_t base58_string [ ] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

uint32_t base58_encode ( const uint8_t *message, uint32_t length, uint8_t *base58 )
{
    // Skip & count leading zeroes.
    uint32_t zeroes        = 0;
    uint32_t base58_length = 0;
    uint32_t size          = length * 138 / 100 + 1; // log(256) / log(58), rounded up.
	uint8_t *p             = ( uint8_t * ) message;	
	
	uint8_t tmp [ 4096 ] = { 0 };
	
    while ( p != ( message + length ) )
	{
        int32_t carry = *p;
        // Apply "b58 = b58 * 256 + ch".
		uint8_t *b58 = ( base58 + size );
		uint32_t i   = 0;
		for ( i = 0; ( carry != 0 ) || ( i < base58_length ) && ( b58 != base58 ); i++, b58-- )
		{
            carry  += 256 * ( *b58 );
            *b58    = carry % 58;
            carry  /= 58;
        }
        assert ( carry == 0 );
        base58_length = i;
		++p;
    }
    // Skip leading zeroes in base58 result.
    p = base58 + ( size - base58_length );
	
	return base58_length;
}



uint32_t base58_decode ( int8_t *base58, uint32_t length, uint8_t *message )
{
	return 0;
}

#ifdef __cplusplus
}
#endif

#endif // !__BASE58_H__