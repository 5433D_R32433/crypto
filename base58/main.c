#include "base58.h"
#include <stdio.h>




int main ( int argc, char **argv )
{
	uint8_t base58 [ 4096 ] = { 0 };
	uint32_t length = base58_encode ( "Hello World", strlen ( "hello world" ), base58 );
	
	for ( uint32_t i = 0; i < length; i++ )
	{
		printf ( "%02X", base58 [ i ] );
	}
	puts ( "" );
	
	return 0;
}