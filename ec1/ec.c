#include <stdio.h>
#include <gmp.h>
#include <stdint.h>


typedef struct ec_t 
{
	mpz_t a;
	mpz_t b;
	mpz_t p;
	
} ec_t;


typedef struct point_t
{
	mpz_t x;
	mpz_t y;
	
} point_t;

ec_t ec = { 0 };


void ec_point_doubling ( point_t *r, point_t p )
{
	mpz_t slope;   // Lambda
	mpz_t tmp;
	
	mpz_init ( slope );
	mpz_init (  tmp  );
	
	if ( mpz_cmp_ui ( p.y, 0 ) != 0 )
	{
		// slope(lambda) = (3x^2) modinv (2y) ( mod p )
		mpz_mul_ui ( tmp,   p.y,   2      );
		mpz_invert ( tmp,   tmp, ec.p     );		
		mpz_mul    ( slope, p.x, p.x      );
		mpz_mul_ui ( slope, slope, 3      );		
        mpz_add    ( slope, slope, ec.a   );
        mpz_mul    ( slope, slope, tmp    );
        mpz_mod    ( slope, slope, ec.p   );
		
		// X_R = slope^2 - 2x ( mod p )
        mpz_mul    ( r->x,  slope, slope  );
        mpz_sub    ( r->x, r->x, p.x      );
        mpz_sub    ( r->x, r->x, p.x      );	
        mpz_mod    ( r->x, r->x, ec.p     );
		
		// Y_R = slope * ( x - X_R ) - y  ( mod p )
        mpz_sub    ( tmp,  p.x,  r->x     );
        mpz_mul    ( r->y, slope, tmp     );
        mpz_sub    ( r->y, r->y,  p.y     );
        mpz_mod    ( r->y, r->y,  ec.p    );
	}
	else
	{
        mpz_set_ui ( r->x, 0 );
        mpz_set_ui ( r->y, 0 );
	}

    mpz_clear ( tmp );
    mpz_clear ( slope );
    return;
}

// slope(lambda) = ( Y_G - Y ) modinv ( X_G - X ) ( mod p )
// X_R = slope(lambda)^2 - x - X_G ( mod p )
// Y_R = slope(lambda) * ( x - X_R ) - y * ( mod p )
void ec_point_addition ( point_t *r, point_t p, point_t q )
{
    mpz_mod ( p.x, p.x, ec.p );
    mpz_mod ( p.y, p.y, ec.p );
    mpz_mod ( q.x, q.x, ec.p );
    mpz_mod ( q.y, q.y, ec.p );

    if ( mpz_cmp_ui ( p.x, 0 ) == 0 && mpz_cmp_ui ( p.y, 0 ) == 0 )
    {
        mpz_set ( r->x, q.x );
        mpz_set ( r->y, q.y );
        return;
    }
    
    if ( mpz_cmp_ui ( q.x, 0 ) == 0 && mpz_cmp_ui ( q.y, 0 ) == 0 )
    {
        mpz_set ( r->x, p.x );
        mpz_set ( r->y, p.y );
        return;
    }
    
    mpz_t tmp;
    mpz_init ( tmp );

    if ( mpz_cmp_ui ( q.y, 0 ) != 0 )
    {
        mpz_sub ( tmp, ec.p, q.y  );
        mpz_mod ( tmp, tmp,  ec.p );
    }
    else
    {
        mpz_set_ui ( tmp, 0 );
    }

    if ( mpz_cmp ( p.y, tmp ) == 0 && mpz_cmp ( p.x, q.x ) == 0 )
    {
        mpz_set_ui ( r->x, 0 );
        mpz_set_ui ( r->y, 0 );
        mpz_clear  ( tmp );
        return;
    }

    if ( mpz_cmp ( p.x, q.x ) == 0 && mpz_cmp ( p.y, q.y ) == 0 )
    {
        ec_point_doubling ( r, p );
        mpz_clear         ( tmp    );
        return;
    }
    else
    {
        mpz_t slope;
        mpz_init_set_ui ( slope, 0              );
		
        mpz_sub         ( tmp, p.x, q.x         );
        mpz_mod         ( tmp, tmp, ec.p        );
        mpz_invert      ( tmp, tmp, ec.p        );
        mpz_sub         ( slope, p.y, q.y       );
        mpz_mul         ( slope, slope, tmp     );
        mpz_mod         ( slope, slope,   ec.p  );
        mpz_mul         ( r->x, slope,  slope   );
        mpz_sub         ( r->x, r->x, p.x       );
        mpz_sub         ( r->x, r->x, q.x       );
        mpz_mod         ( r->x, r->x, ec.p      );
        mpz_sub         ( tmp, p.x, r->x        );
        mpz_mul         ( r->y, slope, tmp      );
        mpz_sub         ( r->y, r->y, p.y       );
        mpz_mod         ( r->y, r->y, ec.p      );

        mpz_clear ( tmp   );
        mpz_clear ( slope );
        return;
    }
}



void ec_scalar_multiplication ( point_t *r, point_t p, mpz_t n )
{
    point_t q = { 0 };
    point_t t = { 0 };

    mpz_init ( q.x );
    mpz_init ( q.y );

    mpz_init ( t.x );
    mpz_init ( t.y );

    uint32_t number_of_bits;
    number_of_bits = mpz_sizeinbase ( n, 2 );
    mpz_set_ui ( r->x, 0 );
    mpz_set_ui ( r->y, 0 );
	
    if ( mpz_cmp_ui ( n, 0 ) == 0 )
    {
        return;
    }
	
    mpz_set ( q.x, p.x );
    mpz_set ( q.y, p.y );
	
    if ( mpz_tstbit ( n, 0 ) == 1 )
    {
        mpz_set ( r->x, p.x );
        mpz_set ( r->y, p.y );
    }
  
    for ( uint32_t i = 0; i < number_of_bits; i++ )
    {
        mpz_set_ui ( t.x, 0 );
        mpz_set_ui ( t.y, 0 );
        ec_point_doubling ( &t, q );

        mpz_set ( q.x, t.x  );
        mpz_set ( q.y, t.y  );
        mpz_set ( t.x, r->x );
        mpz_set ( t.y, r->y );
		
        if ( mpz_tstbit ( n, i ) == 1 )
        {
            ec_point_addition ( r, t, q );
        }
    }
    
    mpz_clear ( q.x );
    mpz_clear ( q.y );
    mpz_clear ( t.x );
    mpz_clear ( t.y );
	
    return;
}


int main ( int argc, char **argv )
{
    mpz_init ( ec.a );
    mpz_init ( ec.b );
    mpz_init ( ec.p );

    point_t p, r;
    mpz_init_set_ui ( r.x, 0 );
    mpz_init_set_ui ( r.y, 0 );
    mpz_init        ( p.x );
    mpz_init        ( p.y );

    mpz_t n;
    mpz_init ( n );

#if 0	
	/*
	 *  Curve domain parameters:
	 *	p:	dfd7e09d5092e7a5d24fd2fec423f7012430ae9d
	 *	a:	dfd7e09d5092e7a5d24fd2fec423f7012430ae9a
	 *	b:	01914dc5f39d6da3b1fa841fdc891674fa439bd4
	 *	N:	00dfd7e09d5092e7a5d25167ecfcfde992ebf8ecad
	 */
	
	mpz_set_str ( ec.p, "0xdfd7e09d5092e7a5d24fd2fec423f7012430ae9d", 0 );
	mpz_set_str ( ec.a, "0xdfd7e09d5092e7a5d24fd2fec423f7012430ae9a", 0 );
	mpz_set_str ( ec.b, "0x01914dc5f39d6da3b1fa841fdc891674fa439bd4", 0 );
	
	/*
		Base point:
		Gx:	70ee7b94f7d52ed6b1a1d3201e2d85d3b82a9810
		Gy:	0b23823cd6dc3df20979373e5662f7083f6aa56f
	*/
	
	mpz_set_str ( p.x, "0x70ee7b94f7d52ed6b1a1d3201e2d85d3b82a9810", 0 );
	mpz_set_str ( p.y, "0x0b23823cd6dc3df20979373e5662f7083f6aa56f", 0 );
	/*
		known verified R point for second curve
		R.x	5432bddd1f97418147aff016eaa6100834f2caa8
		R.y	c498b88965689ee44df349b066cd43cbf4f2c5d0
		problem is found discrete logaritm, unknown k
		so just set the same...
	*/	
	mpz_set_str ( n, "0x00542d46e7b3daac8aeb81e533873aabd6d74bb710", 0 );
#endif

    /*
	 *  Curve domain parameters secp192k1:
	 *  E: y ^ 2 = x ^ 3 + ax + b
	 *
	 *	p:	2 ^ 192 − 2 ^ 32 − 2 ^ 12 − 2 ^ 8 − 2 ^ 7 − 2 ^ 6 − 2 ^ 3 − 1 = FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFE FFFFEE37
	 *	a:	00000000 00000000 00000000 00000000 00000000 00000000
	 *	b:	00000000 00000000 00000000 00000000 00000000 00000003
	 *	N:	FFFFFFFF FFFFFFFF FFFFFFFE 26F2FC17 0F69466A 74DEFD8D
	 *  h ( cofactor ): 01
	 */
	 
	mpz_set_str ( ec.p, "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFEE37", 0 );
	mpz_set_str ( ec.a, "0x000000000000000000000000000000000000000000000000", 0 );
	mpz_set_str ( ec.b, "0x000000000000000000000000000000000000000000000003", 0 );	
	
	mpz_set_str ( p.x,  "0xDB4FF10EC057E9AE26B07D0280B7F4341DA5D1B1EAE06C7D", 0 );
	mpz_set_str ( p.y,  "0x9B2F2F6D9C5628A7844163D015BE86344082AA88D95E2F9D", 0 );
	
	mpz_set_str ( n,    "0xFFFFFFFFFFFFFFFFFFFFFFFE26F2FC170F69466A74DEFD8D", 0 );
	
	
	/*	p = k x G == R = n x P	*/
	ec_scalar_multiplication ( &r, p, n );
	
	printf ( "\nX: ");
	mpz_out_str ( stdout, 16, r.x ); 
	printf ( "\nY: ");
	mpz_out_str ( stdout, 16, r.y ); 
	printf ( "\n" );

	mpz_clear ( ec.a ); 
	mpz_clear ( ec.b ); 
	mpz_clear ( ec.p );
	
	mpz_clear ( r.x  );
	mpz_clear ( r.y  );
	
	mpz_clear ( p.x  ); 
	mpz_clear ( p.y  );
	mpz_clear ( n );  

	
    return 0;
}
