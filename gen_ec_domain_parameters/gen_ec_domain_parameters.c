#include <stdio.h>
#include <gmp.h>
#include <stdint.h>

/*
 * 1. m = field size and power or the leading x of the irreducible polynomial
 * 2. f(x) = irreducible polynomial modulus for polynomial basis
 * 3. a = coefficient for the elliptic curve equation
 * 4. b = coefficient for the elliptic curve equation, b does not equal zero
 * 5. P = (xp, yp), a point on the elliptic curve
 * 6. n = the order of the point, P
 * 7. h = the cofactor, such that h = #E( F( 2^m ) ) / n
 * 		h = { 2, 4 }
 * 8. s = seed for the hash function for random parameter generation
 */
 
 
 /* The field size is related to the
  * chosen security level. At a minimum, the field size should be ≥ 2163, where
  * 163 denotes the smallest NIST standard value.
  */
 
 // number of points: pow ( 2, m ) + 1- sqrt ( pow ( 2, m ) ) ≤ #E ( F( pow ( 2, m ) ) ) ≤ pow ( 2, m ) + 1 + sqrt ( pow ( 2, m ) )

// Lucas Sequence


int main ( int argc, char **argv )
{
	
	
	
	
	
	
	return 0;
}