#ifndef __SHA2_H__
#define __SHA2_H__

/*
 * FIPS 180-2 SHA-224/256/384/512 implementation
 * Copyright (C) 2023, Saeed Rezaee <saeed@rezaee.net>
 * All rights reserved.
 */
 
 #include <stdint.h>
 
#ifdef __cplusplus
extern "C" {
#endif

#define SHA224_DIGEST_SIZE ( 224 / 8 )
#define SHA256_DIGEST_SIZE ( 256 / 8 )
#define SHA384_DIGEST_SIZE ( 384 / 8 )
#define SHA512_DIGEST_SIZE ( 512 / 8 )

#define SHA256_BLOCK_SIZE  (  512 / 8 )
#define SHA512_BLOCK_SIZE  ( 1024 / 8 )
#define SHA384_BLOCK_SIZE  SHA512_BLOCK_SIZE
#define SHA224_BLOCK_SIZE  SHA256_BLOCK_SIZE


#define RSH(x, n)    ( x >> n )
#define LSH(x, n)    ( x << n )
#define ROR(x, n)    ( ( x >> n ) | ( x << ( ( sizeof ( x ) << 3 ) - n ) ) )
#define ROL(x, n)    ( ( x << n ) | ( x >> ( ( sizeof ( x ) << 3 ) - n ) ) )
#define CH(x, y, z)  ( ( x & y ) ^ ( ~x & z ) )
#define MAJ(x, y, z) ( ( x & y ) ^ (  x & z ) ^ ( y & z ) )

#define SHA256_F1(x) ( ROR ( x,  2 ) ^ ROR ( x, 13 ) ^ ROR ( x, 22 ) )
#define SHA256_F2(x) ( ROR ( x,  6 ) ^ ROR ( x, 11 ) ^ ROR ( x, 25 ) )
#define SHA256_F3(x) ( ROR ( x,  7 ) ^ ROR ( x, 18 ) ^ RSH ( x,  3 ) )
#define SHA256_F4(x) ( ROR ( x, 17 ) ^ ROR ( x, 19 ) ^ RSH ( x, 10 ) )

#define SHA512_F1(x) ( ROR ( x, 28 ) ^ ROR ( x, 34 ) ^ ROR ( x, 39 ) )
#define SHA512_F2(x) ( ROR ( x, 14 ) ^ ROR ( x, 18 ) ^ ROR ( x, 41 ) )
#define SHA512_F3(x) ( ROR ( x,  1 ) ^ ROR ( x,  8 ) ^ RSH ( x,  7 ) )
#define SHA512_F4(x) ( ROR ( x, 19 ) ^ ROR ( x, 61 ) ^ RSH ( x,  6 ) )

#define UNPACK32(x, s)                            \
{                                                 \
    *( ( s ) + 3 ) = ( uint8_t ) ( ( x )       ); \
    *( ( s ) + 2 ) = ( uint8_t ) ( ( x ) >>  8 ); \
    *( ( s ) + 1 ) = ( uint8_t ) ( ( x ) >> 16 ); \
    *( ( s ) + 0 ) = ( uint8_t ) ( ( x ) >> 24 ); \
}

#define PACK32(s, x)                                \
{                                                   \
    *(x) =   ( ( uint32_t ) *( ( s ) + 3 )       )  \
           | ( ( uint32_t ) *( ( s ) + 2 ) <<  8 )  \
           | ( ( uint32_t ) *( ( s ) + 1 ) << 16 )  \
           | ( ( uint32_t ) *( ( s ) + 0 ) << 24 ); \
}

#define UNPACK64(x, s)                            \
{                                                 \
    *( ( s ) + 7 ) = ( uint8_t ) ( ( x )       ); \
    *( ( s ) + 6 ) = ( uint8_t ) ( ( x ) >>  8 ); \
    *( ( s ) + 5 ) = ( uint8_t ) ( ( x ) >> 16 ); \
    *( ( s ) + 4 ) = ( uint8_t ) ( ( x ) >> 24 ); \
    *( ( s ) + 3 ) = ( uint8_t ) ( ( x ) >> 32 ); \
    *( ( s ) + 2 ) = ( uint8_t ) ( ( x ) >> 40 ); \
    *( ( s ) + 1 ) = ( uint8_t ) ( ( x ) >> 48 ); \
    *( ( s ) + 0 ) = ( uint8_t ) ( ( x ) >> 56 ); \
}

#define PACK64(s, x)                                \
{                                                   \
    *(x) =   ( ( uint64_t ) *( ( s ) + 7 )       )  \
           | ( ( uint64_t ) *( ( s ) + 6 ) <<  8 )  \
           | ( ( uint64_t ) *( ( s ) + 5 ) << 16 )  \
           | ( ( uint64_t ) *( ( s ) + 4 ) << 24 )  \
           | ( ( uint64_t ) *( ( s ) + 3 ) << 32 )  \
           | ( ( uint64_t ) *( ( s ) + 2 ) << 40 )  \
           | ( ( uint64_t ) *( ( s ) + 1 ) << 48 )  \
           | ( ( uint64_t ) *( ( s ) + 0 ) << 56 ); \
}


typedef struct sha256_t 
{
    uint32_t total_length;
    uint32_t length;
    uint8_t  block [ 2 * SHA256_BLOCK_SIZE ];
    uint32_t h     [ 8 ];
	
} sha256_t;

typedef struct sha512_t 
{
    uint32_t total_length;
    uint32_t length;
    uint8_t  block [ 2 * SHA512_BLOCK_SIZE ];
    uint64_t h     [ 8 ];
	
} sha512_t;

typedef sha512_t sha384_t;
typedef sha256_t sha224_t;

void sha224_init   ( sha224_t *ctx                                            );
void sha224_update ( sha224_t *ctx, const uint8_t *message, uint32_t length   );
void sha224_final  ( sha224_t *ctx, uint8_t *digest                           );
void sha224        ( const uint8_t *message, uint32_t length, uint8_t *digest );

void sha256_init   ( sha256_t *ctx                                            );
void sha256_update ( sha256_t *ctx, const uint8_t *message, uint32_t length   );
void sha256_final  ( sha256_t *ctx, uint8_t *digest                           );
void sha256        ( const uint8_t *message, uint32_t length, uint8_t *digest );

void sha384_init   ( sha384_t *ctx                                            );
void sha384_update ( sha384_t *ctx, const uint8_t *message, uint32_t length   );
void sha384_final  ( sha384_t *ctx, uint8_t *digest                           );
void sha384        ( const uint8_t *message, uint32_t length, uint8_t *digest );

void sha512_init   ( sha512_t *ctx                                            );
void sha512_update ( sha512_t *ctx, const uint8_t *message, uint32_t length   );
void sha512_final  ( sha512_t *ctx, uint8_t *digest                           );
void sha512        ( const uint8_t *message, uint32_t length, uint8_t *digest );







#ifdef __cplusplus
}
#endif

#endif /* !__SHA2_H__ */