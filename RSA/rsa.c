#include <stdio.h>
#include <gmp.h>
#include <stdint.h>

// $ openssl genrsa -out key.pem 1024
// $ openssl rsa -in key.pem -text

void generate_number ( uint64_t n, mpz_t *result )
{
    mpz_t base;
    mpz_t min, max;
    
    // ( 2 ^ ( n - 1 ) ) + 1
    mpz_init_set_ui ( base, 2 );
    mpz_init        ( min );
    mpz_pow_ui      ( min, base, n - 1 );
    mpz_add_ui      ( min, min, 1 );
    
    printf ( "min => %s\n", mpz_get_str ( NULL, 10, min ) );
    
    
    // ( 2 ^ n ) - 1
    mpz_init        ( max );
    mpz_pow_ui      ( max, base, n );
    mpz_sub_ui      ( max, max, 1 );
    
    printf ( "max => %s\n", mpz_get_str ( NULL, 10, max ) );
    
    
    mpz_clear ( base );
    mpz_clear ( min  );
    mpz_clear ( max  );
    
    return;
}

void generate_random_number ( mpz_t *r, const mpz_t n )
{
	gmp_randstate_t state;
	gmp_randinit_default ( state );

#if defined(_WIN32) || defined(_WIN64)
	// uint64_t ticks;
    // QueryPerformanceCounter ( ( LARGE_INTEGER* ) &ticks);
	// gmp_randseed_ui      ( state, ticks );
	uint64_t seed;
	BCRYPT_ALG_HANDLE hAlgorithm = 0;
	BCryptOpenAlgorithmProvider ( &hAlgorithm, L"RNG", 0, 0 );
	// CryptGenRandom ( hCryptProvider,  8,  ( uint8_t* ) &seed );
	BCryptGenRandom ( hAlgorithm, ( uint8_t* ) &seed, 8, 0 );
	gmp_randseed_ui   ( state, seed );
	mpz_t m;
	mpz_init   ( m );
	mpz_sub_ui ( m, n, 1 );
#else
	gmp_randseed_ui   ( state, time ( 0 ) );
#endif
	mpz_urandomm  ( *r, state, m );
	gmp_randclear ( state );
}

void generate_random_number_bits ( mpz_t *r, uint32_t bits )
{
	gmp_randstate_t state;
	gmp_randinit_default ( state );
	uint8_t  rnd   [ 16 ] = { 0 };
	int8_t  buffer [ 32 ] = { 0 };
	mpz_t seed;
#if defined(_WIN32) || defined(_WIN64)

	BCRYPT_ALG_HANDLE hAlgorithm = 0;
	BCryptOpenAlgorithmProvider ( &hAlgorithm, L"RNG", 0, 0 );
	BCryptGenRandom             (  hAlgorithm, rnd, sizeof ( rnd ), 0  );
	
	for ( uint32_t i = 0; i < sizeof ( rnd ); i++ )
	{
		snprintf ( buffer, sizeof ( buffer ), "%02X", rnd [ i ] );
	}
	
	gmp_init_set_str ( seed, buffer, 16 );
	gmp_randseed     ( state,  seed     );
#else 
	gmp_randseed_ui   ( state, time ( 0 ) );
#endif

	mpz_urandomb  ( *r, state, ( mp_bitcnt_t ) bits );
	gmp_randclear ( state );
	mgz_clear     ( seed  );
}


void generate_prime_number_bits ( mpz_t *r, uint64_t number_of_bits )
{	
	mpz_t n;
	mpz_init ( n );
	generate_random_number_bits ( &n, number_of_bits );
	mpz_nextprime ( *r, n );
	mpz_clear ( n );
}


void generate_prime_number ( mpz_t *r, mpz_t n )
{	
	mpz_t p;
	mpz_init               (  p );
	generate_random_number ( &p, n );
	mpz_nextprime          ( *r, p );
	mpz_clear 	           (  p );
}



// Fermat's little theorem states that pow ( a, p ) = a mod ( p )
// Euler's Theorem If n ≥ 1 and gcd( a, n ) = 1, then aφ(n) ≡ 1 ( mod n ). 


// prime1: p
// prime2: q
// modulus: N = p * q
// publicExponent: e
// privateExponent: d

// Chinese remainder algorithm

// exponent1   = dp
// exponent2   = dq
// coefficient = qInv

// dp   = d mod ( p − 1 )
// dq   = d mod ( q − 1 )
// qInv = q ^ −1 mod ( p ) 

// Euler-Fermat generalisation, which states:

// a ^ ϕ(n) = 1 mod n



void eulers_totient ( )
{
	
}


void carmichael_totient ( )
{
	
}


// (m^e)^d = m ( mod n )
// (m^d)^e = m ( mod n )

void rsa_encrypt ( int8_t *message, uint32_t length, mpz_t public_key )
{
	
}


void rsa_decrypt ( int8_t *cipher, uint32_t length, mpz_t private_key )
{
	
}


void lehmers_conjecture ( )
{
}


void riemann_hypothesis ( )
{
}

void carmichael_conjecture ( )
{
}

void eulers_theorem ( )
{
}

void lagranges_theorem ( )
{
}

void fermat_little_theorem ( )
{
}


int main ( int argc, char **argv )
{
	
	mpz_t q;
	mpz_t p;
	
	mpz_t ptmp;
	mpz_t qtmp;
	
	mpz_t modulus;
	mpz_t phi;
	
	mpz_t e;
	mpz_t d;
	
	mpz_t gcd;
	
	// Modulus: N = pq
	mpz_init ( modulus );
	mpz_init_set_str ( p, "0x00f2a30703bce2a8d180ed773bfb9646429c475d404ca8a407772be63619ba520396e5d1d80d56d8c39894ac12ad58b12b2998c2eb8cdbf98cd9748ae502b7adc9", 0 );
	mpz_init_set_str ( q, "0x00ca2ba36e74e6e2c89493be3d801bfd01967d60195f7266eaded7c9dcee79f45d44f96e883034f2c6e255446f781d421c3972545e0b624f5c971721d7a383957f", 0 );
	mpz_mul ( modulus, p, q );	

	// Euler's Totient for Prime Number: ϕ(p) = ( p - 1 )
	// comprime: pairs of numbers that do not have any common factor other than 1
	// 7 is Prime Number => coprimes = { 1, 2, 3, 4, 5, 6 };
	// 11 is Prime Number => coprimes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	// Euler's Totient ϕ(n) = n * ( 1 - 1/n )
	// ϕ(pq) = ϕ(p) ϕ(q) = (p−1)(q−1) = coprimes numbers between p and q
	mpz_init ( phi  );
	mpz_init ( ptmp );
	mpz_init ( qtmp ); 
	
	// Carmichael's Totient λ(n) = 
	
	mpz_sub_ui ( ptmp, p, 1 );
	mpz_sub_ui ( qtmp, q, 1 );
	mpz_mul    ( phi, ptmp, qtmp );
	
	
	// e => gcd ( e, ϕ ( pq ) ) = 1, 1 < e < ϕ ( pq ) = AKA generator
	mpz_init_set_ui ( e, 65537 );
	mpz_init ( gcd );
	mpz_gcd  ( gcd, phi, e );
	
	// make sure gcd of phi and e is 1
	int count = 1;
	while ( mpz_cmp_ui ( gcd, 1 ) != 0 )
	{
		printf ( "Count: %d", count++ );
		mpz_add_ui ( e, e, 2 );
		mpz_gcd ( gcd, phi, e );
	}
	
	
	// d ≡ e ^ −1 (mod λ(n))
	mpz_init         ( d );	
	mpz_t minus_one;
	mpz_init_set_str ( minus_one, "-1", 10 );
	mpz_powm         ( d, e, minus_one, phi );

	
#if 0	
	if ( mpz_invert ( d, e, phi ) == 0 ) 
	{
		printf ( "Failure inverting key.\n"  );
	}
#endif
	
	mpz_t exponent1;
	mpz_t exponent2;
	mpz_t coefficient;
	
	mpz_init ( exponent1   );
	mpz_init ( exponent2   );
	mpz_init ( coefficient );
	
	mpz_mod  ( exponent1, d, ptmp );
	mpz_mod  ( exponent2, d, qtmp );
	mpz_powm ( coefficient, q, minus_one, p );
	
	
	
	
	printf ( "<<<PRIVATE KEY>>>\n" );
	
	printf ( "modulus ( N ) => 0x%s\n", mpz_get_str ( NULL, 16, modulus ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "publicExponent ( e ) => %s ( 0x%s )\n", mpz_get_str ( NULL, 10, e ), mpz_get_str ( NULL, 16, e ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "privateExponent ( d ) => 0x%s\n", mpz_get_str ( NULL, 16, d ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "prime1 ( p ) => 0x%s\n", mpz_get_str ( 0, 16, p ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
    printf ( "prime2 ( q ) => 0x%s\n", mpz_get_str ( 0, 16, q           ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "gcd => 0x%s\n", mpz_get_str          ( 0, 16, gcd         ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "phi => 0x%s\n", mpz_get_str          ( 0, 16, phi         ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "exponent1 => 0x%s\n", mpz_get_str    ( 0, 16, exponent1   ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "exponent2 => 0x%s\n", mpz_get_str    ( 0, 16, exponent2   ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
	printf ( "coefficient => 0x%s\n", mpz_get_str  ( 0, 16, coefficient ) );
	printf ( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" );
	
    mpz_clear ( p           );
	mpz_clear ( q           );
	mpz_clear ( ptmp        );
	mpz_clear ( qtmp        );
	mpz_clear ( e           );
	mpz_clear ( d           );
	mpz_clear ( phi         );
	mpz_clear ( modulus     );
	mpz_clear ( exponent1   );
	mpz_clear ( exponent2   );
	mpz_clear ( coefficient );
    
    return 0;
}