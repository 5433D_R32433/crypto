#include <stdio.h>
#include <gmp.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include <windows.h>


int win32_get_random ( uint8_t *buffer, uint16_t length )
{
	int32_t ret;
	HCRYPTPROV hCryptProv = 0;
	if ( CryptAcquireContext ( &hCryptProv, 
							   0, 
							   0, 
							   PROV_RSA_FULL, CRYPT_VERIFYCONTEXT ) == FALSE ) 
	{
		return -1;
	}
	if ( CryptGenRandom ( hCryptProv, length, buffer ) == FALSE ) 
	{
		CryptReleaseContext ( hCryptProv, 0 );
		return -1;
	}
	CryptReleaseContext ( hCryptProv, 0 );
	return 0;
}


/* Montgomery curve */
/* M_{A,B}:By^2 = x^3 + Ax^2 + x */
/* y^2 = x^3 + 486662 * x^2 + x */


/*
 * P = 2^255 - 19 => 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFED
 * A = 486662
 * B = 0
 * N = 2^252 + 27742317777372353535851937790883648493 => 0x1000000000000000000000000000000014DEF9DEA2F79CD65812631A5CF5D3ED
 * Gx = 9
 * Gx = N/A
 * h (cofactor) = 8
 */



int main ( int argc, char **argv )
{
	mpz_t p;
	mpz_init_set_ui  ( p, 2      );
	mpz_pow_ui       ( p, p, 255 );
	mpz_sub_ui       ( p, p, 19  );
	
	mpz_t t;
	mpz_init_set_str ( t, "27742317777372353535851937790883648493", 10 );
	
	mpz_t n;
	mpz_init_set_ui  ( n, 2      );
	mpz_pow_ui       ( n, n, 252 );
	mpz_add          ( n, n, t   );
	
	printf           ( "P: %s\n", mpz_get_str ( 0, -16, p ) );
	printf           ( "N: %s\n", mpz_get_str ( 0, -16, n ) );
	
	uint8_t buffer [ 16 ] = { 0 };
	win32_get_random ( ( uint8_t* ) &buffer, 16 );
	
	for ( int i = 0; i < 16; i++ )
	{
		printf ( "%X", buffer [ i ] );
	}
	printf ( "\n" );
	

	return 0;
}