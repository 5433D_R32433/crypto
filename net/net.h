#ifndef __NET_H__
#define __NET_H__

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib,"Ws2_32.lib")
void winsock_init ( )
{
	WSADATA wsa = { 0 };
	int result = WSAStartup ( MAKEWORD ( 2, 2 ), &wsa );
    if ( !result ) 
	{
        fprintf ( stderr, "WSAStartup failed: %d\n", result );
        return;
    }
}

void winsock_deinit ( )
{
	WSACleanup ( );
	return;
}

typedef SOCKET socket_t;

#else
	
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <netdb.h>
#include <errno.h>
typedef int socket_t;

#if defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
#include <sys/endian.h>
#elif defined(__APPLE__)
#include <machine/endian.h>
#elif defined(sun)
#include <sys/isa_defs.h>
#else
#include <endian.h>
#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <time.h>


int net_tcp_connect ( socket_t *fd, const char *host, int port )
{
    struct sockaddr_in server_addr;
    struct hostent    *server_host;

#if defined(_WIN32) || defined(_WIN32_WCE)
	winsock_init ( );
#else
	signal ( SIGPIPE, SIG_IGN );
#endif
	if ( ( server_host = gethostbyname ( host ) ) == 0 )
	{
		return ( NET_ERROR_UNKNOWN_HOST );
	}
	
	if ( ( *fd = socket ( AF_INET, SOCK_STREAM, IPPROTO_IP ) ) < 0 )
	{
		return ( NET_ERROR_SOCKET_FAILED );
	}
	
	memcpy ( ( void* ) &server_addr.sin_addr, ( void* ) server_host->h_addr, server_host->h_length );
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_port   = htons ( port );
	
	if ( connect ( *fd, ( struct sockaddr * ) &server_addr, sizeof ( server_addr ) ) < 0 )
	{
	    closesocket ( *fd );
	    return ( NET_ERROR_CONNECT_FAILED );
	}	
	
	return ( 0 );
}

int net_tcp_bind ( socket_t *fd, const char *bind_ip, int port )
{
   int n, c [ 4 ];
   struct sockaddr_in server_addr;

#if defined(_WIN32) || defined(_WIN32_WCE)
	winsock_init ( );
#else
   signal ( SIGPIPE, SIG_IGN );
#endif

   if ( ( *fd = socket ( AF_INET, SOCK_STREAM, IPPROTO_IP ) ) < 0 )
   {
	   return ( NET_ERROR_SOCKET_FAILED );
   }
   
   n = 1;
   setsockopt ( *fd, SOL_SOCKET, SO_REUSEADDR, ( const char* ) &n, sizeof ( n ) );
   server_addr.sin_addr.s_addr = INADDR_ANY;
   server_addr.sin_family      = AF_INET;
   server_addr.sin_port        = net_htons( port );

   if( bind_ip != 0 )
   {
       memset ( c, 0, sizeof ( c ) );
       sscanf ( bind_ip, "%d.%d.%d.%d", &c [ 0 ], &c [ 1 ], &c [ 2 ], &c [ 3 ] );

       for ( n = 0; n < 4; n++ )
	   {
		   if ( c [ n ] < 0 || c [ n ] > 255 )
		   {
               break;
		   }
	   }

       if ( n == 4 )
	   {
		   server_addr.sin_addr.s_addr = ( ( unsigned long ) c [ 0 ] << 24 ) |
										 ( ( unsigned long ) c [ 1 ] << 16 ) |
										 ( ( unsigned long ) c [ 2 ] <<  8 ) |
										 ( ( unsigned long ) c [ 3 ]       );
	   }
   }

   if ( bind ( *fd, ( struct sockaddr* ) &server_addr, sizeof ( server_addr ) ) < 0 )
   {
       closesocket ( *fd );
       return ( NET_ERROR_BIND_FAILED );
   }

   if ( listen ( *fd, NET_LISTEN_BACKLOG ) != 0 )
   {
       closesocket ( *fd );
       return ( NET_ERROR_LISTEN_FAILED );
   }

   return ( 0 );
}

static int net_is_blocking ( void )
{
#if defined(_WIN32) || defined(_WIN32_WCE)
    return ( WSAGetLastError() == WSAEWOULDBLOCK );
#else
    switch ( errno )
    {
#if defined EAGAIN
        case EAGAIN:
#endif
#if defined( EWOULDBLOCK && EWOULDBLOCK != EAGAIN )
        case EWOULDBLOCK:
#endif
            return ( 1 );
    }
    return ( 0 );
#endif
}

int net_tcp_accept ( socket_t bind_fd, socket_t *client_fd, void *client_ip )
{
    struct sockaddr_in client_addr;
#if defined(__socklen_t_defined) || defined(_SOCKLEN_T) || defined(_SOCKLEN_T_DECLARED)
    socklen_t n = ( socklen_t ) sizeof ( client_addr );
#else
    int n = ( int ) sizeof ( client_addr );
#endif
    *client_fd = accept ( bind_fd, ( struct sockaddr* ) &client_addr, &n );
    if ( *client_fd < 0 )
    {
        if ( net_is_blocking() != 0 )
		{
			return ( NET_ERROR_WANT_READ );
		}
        return ( NET_ERROR_ACCEPT_FAILED );
    }
    if ( client_ip )
	{
		memcpy ( client_ip, &client_addr.sin_addr.s_addr, sizeof ( client_addr.sin_addr.s_addr ) );
	}
    return ( 0 );
}

int net_set_block ( socket_t fd )
{
#if defined(_WIN32) || defined(_WIN32_WCE)
    u_long n = 0;
    return ( ioctlsocket ( fd, FIONBIO, &n ) );
#else
    return ( fcntl ( fd, F_SETFL, fcntl ( fd, F_GETFL ) & ~O_NONBLOCK ) );
#endif
}

int net_set_nonblock ( socket_t fd )
{
#if defined(_WIN32) || defined(_WIN32_WCE)
    u_long n = 1;
    return ( ioctlsocket ( fd, FIONBIO, &n ) );
#else
    return ( fcntl ( fd, F_SETFL, fcntl ( fd, F_GETFL ) | O_NONBLOCK ) );
#endif
}


void net_usleep ( unsigned long usec )
{
	struct timeval tv = { 0 };
	tv.tv_sec         =   0;
	tv.tv_usec        =   usec;
	select ( 0, 0, 0, 0, &tv );
}

int net_tcp_recv ( socket_t fd, unsigned char *buffer, size_t length )
{
    int ret = recv ( fd, buffer, length );
    if ( ret < 0 )
    {
        if ( net_is_blocking ( ) != 0 )
		{
			return ( NET_ERROR_WANT_READ );
		}
#if defined(_WIN32) || defined(_WIN32_WCE)
        if ( WSAGetLastError() == WSAECONNRESET )
		{
			return ( NET_ERROR_CONNECTION_RESET );
		}
#else
        if ( errno == EPIPE || errno == ECONNRESET )
		{
			return ( NET_ERROR_CONNECTION_RESET );
		}
        if ( errno == EINTR )
		{
			return ( NET_ERROR_WANT_READ );
		}
#endif
        return ( NET_ERROR_RECV_FAILED );
    }
    return ( ret );
}

int net_tcp_send ( socket_t fd, const unsigned char *buffer, size_t length )
{
	int ret = send ( fd, buffer, length );
    if ( ret < 0 )
    {
        if ( net_is_blocking ( ) != 0 )
		{
			return ( NET_ERROR_WANT_WRITE );
		}
#if defined(_WIN32) || defined(_WIN32_WCE)
        if ( WSAGetLastError ( ) == WSAECONNRESET )
		{
			return ( NET_ERROR_CONNECTION_RESET );
		}
#else
        if ( errno == EPIPE || errno == ECONNRESET )
		{
			return ( NET_ERROR_CONNECTION_RESET );
		}

        if ( errno == EINTR )
		{
			return ( NET_ERROR_CONNECTION_RESET );
		}
#endif
        return ( NET_ERROR_SEND_FAILED );
    }

    return ( ret );
}

void net_tcp_close ( socket_t fd )
{
	shutdown    ( fd, 2 );
	closesocket ( fd );
#if defined(_WIN32) || defined(_WIN32_WCE)
	winsock_deinit ( );
#endif
}


#ifdef __cplusplus
}
#endif
#endif /* !__NET_H__ */