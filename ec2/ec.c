// Point at Infinity is Denoted by (0,0)
#include<stdio.h>
#include<stdlib.h>
#include<gmp.h>


typedef struct ec_t
{
	mpz_t a;
	mpz_t b;
	mpz_t p;
} ec_t;


typedef struct point_t
{
	mpz_t x;
	mpz_t y;
} point_t;


struct ec_t ec = { 0 };


void seclect_ec ( )
{
	printf    ( "\n Enter Elliptic Curve Parameters i.e: a,b and p" );
	gmp_scanf ( "%Zd",&ec.a );
	gmp_scanf ( "%Zd",&ec.b );
	gmp_scanf ( "%Zd",&ec.p );
	return;
}


int main ( int argc, char **argv )
{	
	int choice;
	mpz_init ( ec.a ); 
	mpz_init ( ec.b ); 
	mpz_init ( ec.p );
	
	select_ec ( );
	printf ( "\n Enter Choice of Operation\n"                   );
	printf ( "\n Enter 1 For Point Addition Operation\n"        );
	printf ( "\n Enter 2 For Scalar Multiplication Operation\n" );
	scanf  ( "%d", &choice );
	
	point_t p, r;
	mpz_init ( p.x ); 
	mpz_init ( p.y );
	
	mpz_init_set_ui ( r.x, 0 ); 
	mpz_init_set_ui ( r.y, 0 );
	
	printf ( "\nEnter Points P(x,y) and/or Q(x,y) to be Added\n" );
	gmp_scanf ( "%Zd", &p.x );
	gmp_scanf ( "%Zd", &p.y );
	
	if ( choice == 1 )
	{
		point_t q;
		mpz_init ( q.x ); 
		mpz_init ( q.y );
		
		gmp_scanf ( "%Zd",&q.x );
		gmp_scanf ( "%Zd",&q.y );
		ec_point_addition ( &r, p, q );
	}
	else
	{
		printf ( "\nEnter n to Find nP\n" );
		mpz_t n;
		mpz_init  ( n );
		gmp_scanf ( "%Zd",&n );
		ec_scalar_multiplication ( &r, p, n );
	}
	gmp_printf ( "\nResultant Point is %Zd, %Zd", r.x, r.y );
	
	
	mpz_clean ( ec.a );
	mpz_clean ( ec.b );
	mpz_clean ( ec.p );
	
	return 0;
}


void ec_point_doubling ( point_t *r, point_t p )
{
	mpz_t slope;
	mpz_t tmp;
	mpz_init (  tmp  );
	mpz_init ( slope );
	
	if ( mpz_cmp_ui ( p.y, 0 ) != 0 )
	{
		mpz_mul_ui ( tmp,   p.y,   2     );
		mpz_invert ( tmp,   tmp,   ec.p  );
		mpz_mul    ( slope, p.x,   p.x   );
		mpz_mul_ui ( slope, slope, 3     );		
		mpz_add    ( slope, slope, ec.a  );
		mpz_mul    ( slope, slope, tmp   );
		mpz_mod    ( slope, slope, ec.p  );
		mpz_mul    ( r->x,  slope, slope );
		mpz_sub    ( r->x,  r->x,  p.x   );
		mpz_sub    ( r->x,  r->x,  p.x   );
		mpz_mod    ( r->x,  r->x,  ec.p  );
		mpz_sub    ( tmp,   p.x,   r->x  );
		mpz_mul    ( r->y,  slope, tmp   );
		mpz_sub    ( r->y,  r->y,  p.y   );
		mpz_mod    ( r->y,  r->y,  ec.p  );
	}
	else
	{
		mpz_set_ui ( r->x, 0 );
		mpz_set_ui ( r->y, 0 );
	}	
	
	mpz_clear (  tmp  );
    mpz_clear ( slope );
	return;
}


void ec_point_addition ( point_t *r, point_t p, point_t q )
{
	mpz_mod ( p.x, p.x, ec.p );
	mpz_mod ( p.y, p.y, ec.p );
	mpz_mod ( q.x, q.x, ec.p );
	mpz_mod ( q.y, q.y, ec.p );
	
	mpz_t temp, slope;
	mpz_init        ( temp );
	mpz_init_set_ui ( slope, 0 );
	
	if ( mpz_cmp_ui ( p.x, 0 ) == 0 && mpz_cmp_ui ( p.y, 0 ) == 0 )
	{ 
		mpz_set(R->x,Q.x); mpz_set(R->y,Q.y); 
		return;
	}
	
	if(mpz_cmp_ui(Q.x,0)==0 &&
	mpz_cmp_ui(Q.y,0)==0)
	{ mpz_set(R->x,P.x); mpz_set(R->y,P.y);
	return;}
	if(mpz_cmp_ui(Q.y,0)!=0)
	{mpz_sub(temp,EC.p,Q.y);mpz_mod(temp,temp,EC
	.p);}
	else
	mpz_set_ui(temp,0);
	// gmp_printf("\n temp=%Zd\n",temp);
	if(mpz_cmp(P.y,temp)==0 &&
	mpz_cmp(P.x,Q.x)==0)
	{ mpz_set_ui(R->x,0); mpz_set_ui(R->y,0); return;}
	if(mpz_cmp(P.x,Q.x)==0 &&
	mpz_cmp(P.y,Q.y)==0)
	{
	Point_Doubling(P,R);
	return;
	}
	else
	{
	mpz_sub(temp,P.x,Q.x);
	mpz_mod(temp,temp,EC.p);
	mpz_invert(temp,temp,EC.p);
	mpz_sub(slope,P.y,Q.y);
	mpz_mul(slope,slope,temp);
	mpz_mod(slope,slope,EC.p);
	mpz_mul(R->x,slope,slope);
	mpz_sub(R->x,R->x,P.x);
	mpz_sub(R->x,R->x,Q.x);
	mpz_mod(R->x,R->x,EC.p);
	mpz_sub(temp,P.x,R->x);
	mpz_mul(R->y,slope,temp);
	mpz_sub(R->y,R->y,P.y);
	mpz_mod(R->y,R->y,EC.p);
	return;
	}
}


void ec_scalar_multiplication ( point_t *r, point_t p, mpz_t n )
{
	point_t Q,T;
	mpz_init(Q.x); 
	mpz_init(Q.y);
	mpz_init(T.x); 
	mpz_init(T.y);
	
	long number_of_bits;
	
	no_of_bits = mpz_sizeinbase(m,2);
	mpz_set_ui(R->x,0);
	mpz_set_ui(R->y,0);
	
	if(mpz_cmp_ui(m,0)==0)
	{
		return;
	}
	
	mpz_set(Q.x,P.x);
	mpz_set(Q.y,P.y);
	
	if(mpz_tstbit(m,0)==1)
	{
		mpz_set(R->x,P.x);mpz_set(R->y,P.y);
	}
	
	for( int i = 1 ; i < number_of_bits; i++ )
	{
		mpz_set_ui(T.x,0);
		mpz_set_ui(T.y,0);
		Point_Doubling(Q,&T);
		gmp_printf("\n %Zd %Zd %Zd %Zd " ,Q.x,Q.y,T.x,T.y);
		mpz_set(Q.x,T.x);
		mpz_set(Q.y,T.y);
		mpz_set(T.x,R->x);
		mpz_set(T.y,R->y);
		if(mpz_tstbit(m,loop))
		{
			Point_Addition(T,Q,R);
		}
	}
}

