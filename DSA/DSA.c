#include <stdio.h>
#include <gmp.h>
#include <stdint.h>
#include <stdbool.h>

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif // !defined(_WIN32) || defined(_WIN64)


#define UNUSED(x) ( ( void ) x )

typedef struct dsa_t
{
	mpz_t p;
	mpz_t q;
	mpz_t g;
	mpz_t h;
	mpz_t x;
	mpz_t y;
} dsa_t;


typedef struct dsa_signature_t
{
	mpz_t r;
	mpz_t s;
	
} dsa_signature_t;



void generate_random_number ( mpz_t *r, const mpz_t n )
{
	gmp_randstate_t state;
	gmp_randinit_default ( state );
	
	mpz_t m;
	mpz_init   ( m );
	mpz_sub_ui ( m, n, 1 );

#if defined(_WIN32) || defined(_WIN64)
	uint8_t rnd [ 32 ] = { 0 };
	mpz_t seed;
	int8_t buffer [ 128 ] = { 0 };
	
	BCRYPT_ALG_HANDLE hAlgorithm = 0;
	BCryptOpenAlgorithmProvider ( &hAlgorithm, L"RNG", 0, 0 );
	BCryptGenRandom ( hAlgorithm, rnd, 32, 0 );
	int8_t *p = buffer;
	
	for ( uint32_t i = 0; i < 32; i++ )
	{
		snprintf ( p, 128, "%02X", rnd [ i ] );
		p += 2;
	}	
	mpz_init_set_str ( seed, buffer, 16 );
	gmp_randseed    ( state, seed );
	mpz_urandomm    ( *r, state, m );
#else
	gmp_randseed_ui   ( state, time ( 0 ) );
	mpz_urandomm      ( *r, state, m );
#endif
	gmp_randclear ( state );
	mpz_clear     ( m     );
	mpz_clear     ( seed  );
}

void generate_random_number_bits ( mpz_t *r, uint32_t bits )
{
	gmp_randstate_t state;
	gmp_randinit_default ( state );
	uint8_t rnd    [ 32  ] = { 0 };
	int8_t  buffer [ 128 ] = { 0 };
	mpz_t seed;
	
#if defined(_WIN32) || defined(_WIN64)
	BCRYPT_ALG_HANDLE hAlgorithm = 0;
	BCryptOpenAlgorithmProvider ( &hAlgorithm, L"RNG", 0, 0 );
	for ( ;; )
	{
		BCryptGenRandom ( hAlgorithm, rnd, 32, 0 );
		int8_t *p = buffer;	
		for ( uint32_t i = 0; i < 32; i++ )
		{
			snprintf ( p, 128, "%02X", rnd [ i ] );
			p += 2;
		}	
		mpz_init_set_str    ( seed, buffer, 16 );
		gmp_randseed        ( state, seed );
		mpz_urandomb        ( *r, state, ( mp_bitcnt_t ) bits );
		if ( mpz_sizeinbase ( *r, 2 ) == bits )
		{
			break;
		}
	}
#else 
	gmp_randseed_ui   ( state, time ( 0 ) );
#endif
	gmp_randclear ( state );
	mpz_clear     ( seed  );
}


void generate_prime_bits ( mpz_t *r, uint64_t number_of_bits )
{	
	mpz_t n;
	mpz_init                    ( n );
	generate_random_number_bits ( &n, number_of_bits );
	mpz_nextprime               ( *r, n );
	mpz_clear                   ( n );
}


void print_openssl_like ( mpz_t x, const char *name )
{
	char buffer [ 4096 ] = { 0 };
	if ( ( mpz_sizeinbase ( x, 16 ) % 2 ) == 0 )
	{
		mpz_get_str ( buffer, 16, x );
	}
	else
	{
		buffer [ 0 ] = '0';
		mpz_get_str ( buffer + 1, 16, x );	
	}
	
	printf ( "\n%s:\n", name );
	printf ( "    " );
	int nl = 0;
	for ( int i = 0; i < strlen ( buffer ); i += 2 )
	{
		if ( nl == 15 )
		{
			nl = 0;
			printf ( "\n    " );
		}
		printf ( "%c%c:", toupper ( buffer [ i ] ), 
		                  toupper ( buffer [ i + 1 ] ) );
		++nl;
	}
	printf ( "\n\n" );
}


 // g = h ^ ( p − 1 ) / q mod p
dsa_t gendsa ( uint32_t p_bits, uint32_t q_bits )
{
	dsa_t dsa = { 0 };
	mpz_init ( dsa.p );
	mpz_init ( dsa.q );
	mpz_init ( dsa.g );
	mpz_init ( dsa.h );
	mpz_init ( dsa.x );
	mpz_init ( dsa.y );
	
	mpz_t p;
	mpz_t q;
	
	mpz_init ( p );
	mpz_init ( q );
	
	generate_prime_bits ( &q, q_bits );
	generate_prime_bits ( &p, p_bits );
	
	// First Phase
	mpz_t t;
	mpz_init ( t );
	
	mpz_t g;
	mpz_init ( g );
	
	mpz_sub_ui ( t, p, 1 );
	mpz_div    ( t, t, q );
	
	mpz_t h;
	mpz_init_set_ui ( h, 2 );
	
	for ( ;; )
	{
		mpz_powm ( g, h, t, p );
		if ( mpz_cmp_ui ( g, 1 ) == 0 )
		{
			/* mpz_t t;
			 * mpz_init ( t );
			 * mpz_sub  ( t, p, 2 );
			 * generate_random_number ( &h, t );
			 */
			mpz_add_ui ( h, h, 1 );
		}
		else
		{
			break;
		}
	}
	
	printf ( "H: %s\n", mpz_get_str ( 0, 16, h ) );
	
	long bits = mpz_sizeinbase ( p, 2 );
	printf ( "P BITS: %d\n", bits );
	print_openssl_like ( p, "P" );
	
	
	bits = mpz_sizeinbase ( q, 2 );
	printf ( "Q BITS: %d\n", bits );
	print_openssl_like ( q, "Q" );
	
	print_openssl_like ( g, "G" );
	
	// Second Phase
	mpz_t x;
	mpz_init ( x );	
	mpz_sub_ui ( t, q, 1 );	
	generate_random_number ( &x, t );
	print_openssl_like ( x, "X(PRIVATE)" );
	
	mpz_t y;
	mpz_init ( y );	
	mpz_powm ( y, g, x, p );
	print_openssl_like ( y, "Y(PUBLIC)" );
	
	mpz_set ( dsa.p, p );
	mpz_set ( dsa.q, q );
	mpz_set ( dsa.g, g );
	mpz_set ( dsa.h, h );
	mpz_set ( dsa.x, x );
	mpz_set ( dsa.y, y );
	
	mpz_clear ( p );
	mpz_clear ( q );
	mpz_clear ( g );
	mpz_clear ( h );
	mpz_clear ( x );
	mpz_clear ( y );
	
	return dsa;
}


bool fermat_little_theorem ( mpz_t p )
{
	mpz_t a;
	mpz_init_set_ui ( a, 2 );
	
	mpz_t t;
	mpz_init ( t );
	mpz_sub_ui ( t, p, 1 );
	
	mpz_t r;
	mpz_init ( r );
	
	mpz_powm ( r, a, t, p );
	
	if ( mpz_cmp_ui ( r, 1 ) == 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
	
	mpz_clear ( a );
	mpz_clear ( t );
	mpz_clear ( r );
}



dsa_signature_t dsa_sign ( dsa_t *dsa, int8_t *message, uint32_t length )
{
	dsa_signature_t dss = { 0 };
	mpz_t r;
	mpz_t s;
	mpz_t k;
	
	mpz_init   ( r );
	mpz_init   ( s );
	mpz_init   ( k );

	generate_random_number ( &k, dsa->q );
	
	for ( ;; )
	{
		mpz_powm ( r, dsa->g, k, dsa->p );
		mpz_mod  ( r, r, dsa->q );

		if ( ( mpz_cmp_ui ( r, 0 ) == 0 ) || ( mpz_cmp_ui ( s, 0 ) == 0 ) )
		{
			generate_random_number ( &k, dsa->q );
		}
		else
		{
			break;
		}
	}
	
	mpz_set ( dss.r, r );
	mpz_set ( dss.s, s );
	
	mpz_clear ( r );
	mpz_clear ( s );
	mpz_clear ( k );
	
	
	return dss;
}


int main ( int argc, char **argv )
{
	UNUSED ( argc );
	UNUSED ( argv );
	
	dsa_t dsa = { 0 };
	dsa = gendsa ( 2048, 256 );

    return 0;
}


    
#if 0
	mpz_t r;
	mpz_init ( r );
	generate_prime_bits ( &r, 4096 );
	long bits = mpz_sizeinbase ( r, 2 );
	printf ( "bits = %d\n", bits );
	printf ( "PRIME: %s\n", mpz_get_str ( 0, -16, r ) );
#endif