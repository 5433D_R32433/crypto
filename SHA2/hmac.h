#ifndef __HMAC_H__
#define __HMAC_H__

/*
 * HMAC-SHA-224/256/384/512 implementation
 * Copyright (C) 2023, Saeed Rezaee <saeed@rezaee.net>
 * All rights reserved.
 */
 
#include "sha2.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct hmac_sha224_t
{
    sha224_t ctx_inside;
    sha224_t ctx_outside;

    /* for hmac_reinit */
    sha224_t ctx_inside_reinit;
    sha224_t ctx_outside_reinit;

    uint8_t block_ipad [ SHA224_BLOCK_SIZE ];
    uint8_t block_opad [ SHA224_BLOCK_SIZE ];
	
} hmac_sha224_t;

typedef struct hmac_sha256_t
{
    sha256_t ctx_inside;
    sha256_t ctx_outside;

    /* for hmac_reinit */
    sha256_t ctx_inside_reinit;
    sha256_t ctx_outside_reinit;

    uint8_t block_ipad [ SHA256_BLOCK_SIZE ];
    uint8_t block_opad [ SHA256_BLOCK_SIZE ];
	
} hmac_sha256_t;

typedef struct hmac_sha384_t
{
    sha384_t ctx_inside;
    sha384_t ctx_outside;

    /* for hmac_reinit */
    sha384_t ctx_inside_reinit;
    sha384_t ctx_outside_reinit;

    uint8_t block_ipad [ SHA384_BLOCK_SIZE ];
    uint8_t block_opad [ SHA384_BLOCK_SIZE ];
	
} hmac_sha384_t;

typedef struct hmac_sha512_t
{
    sha512_t ctx_inside;
    sha512_t ctx_outside;

    /* for hmac_reinit */
    sha512_t ctx_inside_reinit;
    sha512_t ctx_outside_reinit;

    uint8_t block_ipad [ SHA512_BLOCK_SIZE ];
    uint8_t block_opad [ SHA512_BLOCK_SIZE ];
	
} hmac_sha512_t;

void hmac_sha224_init   ( hmac_sha224_t *ctx, const uint8_t *key, uint32_t key_length         );
void hmac_sha224_reinit ( hmac_sha224_t *ctx );
void hmac_sha224_update ( hmac_sha224_t *ctx, const uint8_t *message, uint32_t message_length );
void hmac_sha224_final  ( hmac_sha224_t *ctx,       uint8_t *mac,     uint32_t mac_length     );
void hmac_sha224        ( const uint8_t *key,       uint32_t key_length,
						  const uint8_t *message,   uint32_t message_length,
						        uint8_t *mac,       uint32_t mac_length );
				 

void hmac_sha256_init   ( hmac_sha256_t *ctx, const uint8_t *key,     uint32_t key_length     );
void hmac_sha256_reinit ( hmac_sha256_t *ctx );
void hmac_sha256_update ( hmac_sha256_t *ctx, const uint8_t *message, uint32_t message_length );
void hmac_sha256_final  ( hmac_sha256_t *ctx,       uint8_t *mac,     uint32_t mac_length     );
void hmac_sha256        ( const uint8_t *key,       uint32_t key_length,
						  const uint8_t *message,   uint32_t message_length,
								uint8_t *mac,       uint32_t mac_length  );


void hmac_sha384_init   ( hmac_sha384_t *ctx, const uint8_t *key, uint32_t key_length         );
void hmac_sha384_reinit ( hmac_sha384_t *ctx );
void hmac_sha384_update ( hmac_sha384_t *ctx, const uint8_t *message, uint32_t message_length );
void hmac_sha384_final  ( hmac_sha384_t *ctx,       uint8_t *mac,     uint32_t mac_length     );
void hmac_sha384        ( const uint8_t *key,     uint32_t key_length,
						  const uint8_t *message, uint32_t message_length,
						        uint8_t *mac,     uint32_t mac_length  );

void hmac_sha512_init   ( hmac_sha512_t *ctx, const uint8_t *key, u   int32_t key_length      );
void hmac_sha512_reinit ( hmac_sha512_t *ctx );
void hmac_sha512_update ( hmac_sha512_t *ctx, const uint8_t *message, uint32_t message_length );
void hmac_sha512_final  ( hmac_sha512_t *ctx,       uint8_t *mac,     uint32_t mac_length     );
void hmac_sha512        ( const uint8_t *key,     uint32_t key_length,
						  const uint8_t *message, uint32_t message_length,
								uint8_t *mac,     uint32_t mac_length );




#ifdef __cplusplus
}
#endif

#endif /* !__HMAC_H__ */