#include "sha2.h"
#include <stdio.h>
#include <stdlib.h>

#if defined(__GNUC__) || defined(__clang__)
#define FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define FORCEINLINE __forceinline
#else
#define FORCEINLINE inline
#endif

#if defined(_MSC_VER)

FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return _byteswap_ushort ( x ); }
FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return _byteswap_ulong  ( x ); }
FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return _byteswap_uint64 ( x ); }

#elif defined (__GNUC__) || defined (__clang__)

FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return __builtin_bswap16 ( x ); }
FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return __builtin_bswap32 ( x ); }
FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return __builtin_bswap64 ( x ); }

#else
	
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )
	
#endif


int main ( int argc, char **argv )
{
	uint32_t h256 [ 8 ] = { 0 };	
	sha256_generate_initial_hash ( h256 );	
	for ( uint32_t i = 0; i < 8; i++ )
	{
		printf ( "0x%X\n", h256 [ i ] );
	}
	printf ( "***********************************************\n" );
	
	uint32_t h224 [ 8 ] = { 0 };	
	sha224_generate_initial_hash ( h224 );	
	for ( uint32_t i = 0; i < 8; i++ )
	{
		printf ( "0x%08X\n", h224 [ i ] );
	}
	printf ( "***********************************************\n" );


	uint32_t k [ 64 ] = { 0 };	
	sha256_generate_k ( k );	
	for ( uint32_t i = 0; i < 64; i++ )
	{
		printf ( "%02d: 0x%08x\n", i, k [ i ] );
	}
	printf ( "***********************************************\n" );
	
	test ( h224 );
	
	return 0;
}