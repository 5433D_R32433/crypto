#include <stdio.h>
#include <stdint.h>

void base64_gen_table ( int8_t *table )
{
	for ( uint32_t i = 0; i < 64; i++ )
	{
		if ( ( i >= 0 ) && ( i < 26 ) )
		{
			table [ i ] = ( int8_t ) ( i + 65 );
		}
		
		if ( ( i >= 26 ) && ( i < 52 ) )
		{
			table [ i ] = ( int8_t ) ( i + 71 );
		}
		
		if ( ( i >= 52 ) && ( i < 62 ) )
		{
			table [ i ] = ( int8_t ) ( i - 4 );
		}
		
		if ( i == 62 )
		{
			table [ i ] = '+';
		}
		
		if ( i == 63 )
		{
			table [ i ] = '/';
		}
	}
}

int32_t base64_encode ( const void *src, 
                        size_t      src_size, 
						int8_t     *dst, 
						size_t      dst_size ) 
{
	int8_t table [ 64 ] = { 0 };
	base64_gen_table ( table );
	
	if ( dst_size < ( 4 * ( ( src_size + 2 ) / 3 ) + 1 ) )
	{
		return -1;
	}
	const uint8_t *in = ( const uint8_t* ) src;
	int8_t        *w  = dst;
	while ( src_size >= 3 ) 
	{
		*w++      = table [ *in >> 2 ];
		*w++      = table [ ( ( *in & 0x03 ) << 4 ) | ( *( in + 1 ) >> 4 ) ];
		*w++      = table [ ( ( *( in + 1 ) & 0x0F ) << 2 ) | ( *( in + 2 ) >> 6 ) ];
		*w++      = table [ *( in + 2 ) & 0x3F ];
		in       += 3;
		src_size -= 3;
	}

	if ( src_size ) 
	{
		*w++ = table [ *in >> 2 ];
		if ( src_size == 1 ) 
		{
			*w++ = table [ ( *in & 0x03 ) << 4 ];
			*w++ = '=';
		} 
		else 
		{ 
			/* size == 2 */
			*w++ = table [ ( ( *in & 0x03 ) << 4 ) | ( *( in + 1 ) >> 4 ) ];
			*w++ = table [ ( *( in + 1 ) & 0x0F ) << 2 ];
		}
		*w++ = '=';
	}
	*w = '\0';
	return ( int32_t )( w - dst );
}

int base64_decode ( const char *src, void *dst, size_t dst_size ) 
{
	const uint8_t *in = ( const uint8_t* ) src;
	uint8_t       *w  = ( uint8_t*       ) dst;
	
	while ( *in && *in != '=' ) 
	{
		uint8_t tab [ 4 ] = { 0, 0, 0, 0 };
		size_t size       = 0;
		
		while ( *in && ( size < 4 ) ) 
		{
			uint8_t c = *in++;
			
			if ( isspace ( c ) )
			{
				continue;
			}
			
			if ( c == '=' )
			{
				break;
			}

			if ( ( c >= 'A' ) && ( c <= 'Z' ) )
			{
				tab [ size++ ] = c - 'A';
			}
			
			else if ( ( c >= 'a' ) && ( c <= 'z' ) )
			{
				tab [ size++ ] = c + 26 - 'a';
			}
			
			else if ('0' <= c && c <= '9')
			{
				tab [ size++ ] = c + 52 - '0';
			}
			
			else if ( c == '+' || c == '-' )
			{
				tab [ size++ ] = 62;
			}
			
			else if ( ( c == '/' ) || ( c == '_' ) )
			{
				tab [ size++ ] = 63;
			}
			
			else
			{
				return -1; /* Invalid character */
			}
		}

		if ( size > 0 ) 
		{
			if ( dst_size < size - 1 )
			{
				return -1;
			}
			
			dst_size -= ( size - 1 );
			*w++ = ( tab [ 0 ] << 2 ) | ( tab [ 1 ] >> 4 );		
			if ( size > 1 ) 
			{
				*w++ = ( tab [ 1 ] << 4 ) | ( tab [ 2 ] >> 2 );
				if ( size > 2 )
				{
					*w++ = ( tab [ 2 ] << 6 ) | tab [ 3 ];
				}
			}
		}
	}
	return ( int32_t ) ( w - ( uint8_t* ) dst );
}


int main ( int argc, char **argv )
{
	int8_t dst [ 512 ] = { 0 };
	const int8_t   *src = "I wanted to tell you that i'm so serious about you and I don't want to lose you!";
	base64_encode ( src, strlen ( src ), dst, 512 );
	printf        ( "base64: %s\n", dst );
	
	int8_t out [ 512 ] = { 0 };
	base64_decode ( dst, out, 512       );
	printf        ( "plain:  %s\n", out );
	return 0;
}