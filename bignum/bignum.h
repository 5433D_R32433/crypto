#ifndef __BIGNUM_H__
#define __BIGNUM_H__


#include <stdint.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct mpi_t
{
    int32_t   s;
    size_t    n;
    uint64_t *p;
} mpi_t;




#ifdef __cplusplus
}
#endif
#endif // !__BIGNUM_H__


#ifdef BIGNUM_IMPLEMENTATION
#ifdef __cplusplus
extern "C" {
#endif

#define CHARS_IN_LIMB  ( sizeof ( uint64_t ) )
#define BITS_IN_LIMB   ( CHARS_IN_LIMBS << 3 )
#define HALF_LIMB_SIZE ( CHARS_IN_LIMB  << 2 )

#define BITS_TO_LIMBS(i)  ( ( ( i ) + BITS_IN_LIMB  - 1 ) / BITS_IN_LIMB  )
#define CHARS_TO_LIMBS(i) ( ( ( i ) + CHARS_IN_LIMB - 1 ) / CHARS_IN_LIMB )

void mpi_init ( mpi_t *x )
{
    if ( !x )
	{
		return;
	}
    x->s = 1;
    x->n = 0;
    x->p = 0;
}

void mpi_free ( mpi_t *x )
{
    if ( !x )
	{
		return;
	}
    if ( x->p )
    {
        memset ( x->p, 0, x->n * CHARS_IN_LIMB );
        free   ( x->p );
    }
    x->s = 1;
    x->n = 0;
    x->p = 0;
	return;
}

void mpi_grow ( mpi_t *x, size_t number_of_limbs )
{
    uint64_t *p = 0;
    if ( nblimbs > MPI_MAX_LIMBS )
	{
		printf ( "Number of limbs exceeded the MPI_MAX_LIMBS: %d\n", MPI_MAX_LIMBS );
		return;
	}
    if ( x->n < number_of_limbs )
    {
        if ( ( p = ( uint64_t* ) malloc ( number_of_limbs * CHARS_IN_LIMB ) == 0 )
		{
			fprintf ( "malloc ( ) failed!\n" );
			return;
		}
        memset ( p, 0, number_of_limbs * CHARS_IN_LIMB );
        if ( x->p )
        {
            memcpy ( p, x->p, x->n * CHARS_IN_LIMB );
            memset ( x->p, 0, x->n * CHARS_IN_LIMB );
            free   ( x->p );
        }
        x->n = number_of_limbs;
        x->p = p;
    }
    return;
}

void mpi_copy ( mpi_t *a, const mpi_t *b )
{
    size_t i = 0;
    if ( a == b )
	{
		return;
	}
    for ( i = b->n - 1; i > 0; i-- )
	{
		if ( b->p [ i ] != 0 )
		{
			break;
		}
	}
    i++;
    a->s = b->s;	
	mpi_grow ( b, i );
    memset ( a->p, 0, a->n * CHARS_IN_LIMB );
    memcpy ( a->p, b->p, i * CHARS_IN_LIMB );
	return;
}

void mpi_swap ( mpi_t *a, mpi_t *b )
{
    mpi_t t = { 0 };
    memcpy ( &t,  a, sizeof ( mpi_t ) );
    memcpy (  a,  b, sizeof ( mpi_t ) );
    memcpy (  b, &t, sizeof ( mpi_t ) );
	return;
	return;
}

int mpi_get_bit ( const mpi_t *x, size_t position )
{
    if ( x->n * BITS_IN_LIMB <= position )
	{
		fprintf ( stderr, "Position exceeded the number of bits in the number!\n" );
		return;
	}
    return ( ( x->p [ position / BITS_IN_LIMB ] >> ( position % BITS_IN_LIMB ) ) & 0x01 );
}

void mpi_set_ui ( mpi_t *r, uint64_t ui )
{
	mpi_grow ( r, 1 );
    memset   ( r->p, 0, r->n * CHARS_IN_LIMB );
    r->p [ 0 ] = ui;
    r->s       = 1;
	return;
}


void mpi_set_si ( mpi_t *r, int64_t si )
{
	mpi_grow ( r, 1 );
    memset   ( r->p, 0, r->n * CHARS_IN_LIMB );
    r->p [ 0 ] = ( si < 0 ) ? -si : si;
    r->s       = ( si < 0 ) ? -1 : 1;
	return;
}

void mpi_set_str ( mpi_t *r, int radix, const char *s )
{
    size_t i      = 0;
	size_t j      = 0;
	size_t length = 0; 
	size_t n      = 0;
    uint64_t d    = 0;
    mpi_t t       = { 0 };

    if ( radix < 2 || radix > 16 )
	{
		fprintf ( stderr, "radix must be between 2 to 16\n" );
		return;
	}
    mpi_init ( &t );
    length = strlen ( s );
    if ( radix == 16 )
    {
        n = BITS_TO_LIMBS ( length << 2 );
        mpi_grow ( r, n );
        mpi_lset ( r, 0 );
        for ( i = length, j = 0; i > 0; i--, j++ )
        {
            if ( i == 1 && s [ i - 1 ] == '-' )
            {
                r->s = -1;
                break;
            }
            mpi_get_digit ( &d, radix, s [ i - 1 ] );
            r->p [ j / ( 2 * CHARS_IN_LIMB ) ] |= d << ( ( j % ( 2 * CHARS_IN_LIMB ) ) << 2 );
        }
    }
    else
    {
        mpi_set_ui ( r, 0 );
        for ( i = 0; i < length; i++ )
        {
            if ( i == 0 && s [ i ] == '-' )
            {
                r->s = -1;
                continue;
            }
            mpi_get_digit ( &d, radix, s [ i ] ) );
            mpi_mul_si    ( &t, r, radix );

            if ( r->s == 1 )
            {
                mpi_add_si ( r, &t, d );
            }
            else
            {
                mpi_sub_si ( r, &t, d );
            }
        }
    }
	return;
}


void mpi_set_bit ( mpi_t *t, size_t position, unsigned char v )
{
    size_t offset = position / BITS_IN_LIMB;
    size_t index  = position % BITS_IN_LIMB;
    if( v != 0 && v != 1 )
	{
		fprintf ( stderr, "The value must be 0 or 1\n" );
		return;
	}      
    if ( r->n * BITS_IN_LIMB <= position )
    {
        if ( v == 0 )
		{
			return;
		}
		mpi_grow ( r, offset + 1 );
    }
    r->p [ offset ] = ( r->p [ offset ] & ~( 0x01 << index ) ) | ( v << index );
}


size_t mpi_size ( const mpi_t *x )
{
    return ( ( mpi_msb ( x ) + 7 ) >> 3 );
}

#ifdef __cplusplus
}
#endif

#endif


